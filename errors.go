package play

import (
	"errors"
)

var (
	ErrQuit = errors.New("quit")
)
