package client

import (
	"sync/atomic"
	"time"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/inpututil"

	"insmo.com/play"
	"insmo.com/play/resource"
	"insmo.com/play/system"
	"insmo.com/play/termlog"
	"insmo.com/play/window"
	"insmo.com/play/world"
)

type Client struct {
	resources *resource.Resources
	world     *world.World
	system    *system.RunSystems
	state     *window.State

	lastUpdate     time.Time
	ready          uint32
	runtime        float64
	resourcesReady uint32
}

func NewClient() *Client {
	w := world.NewWorld()
	world.Assemble(w)
	res := resource.New()
	state := window.NewState("", 768, 576, 1, false)

	c := &Client{
		state:      state,
		world:      w,
		resources:  res,
		system:     system.NewRunSystems(w, res, state),
		lastUpdate: time.Now(),
	}

	go c.loadResources()

	return c
}

func (c *Client) Run() error {
	state := c.state
	return ebiten.Run(
		c.Update,
		int(state.Size().W()),
		int(state.Size().H()),
		float64(state.Scale()),
		state.Title(),
	)
}

func (c *Client) Update(screen *ebiten.Image) error {
	now := time.Now()
	f64 := now.Sub(c.lastUpdate).Seconds()
	dt := float32(f64)
	c.runtime += f64
	c.lastUpdate = now
	c.state.Sync(screen)

	if inpututil.IsKeyJustPressed(ebiten.KeyQ) {
		return play.ErrQuit
	}

	err := c.system.Update(dt)

	if err != nil {
		return err
	}

	if !c.isReady() || ebiten.IsRunningSlowly() {
		return nil
	}

	err = c.system.Render(screen, dt)

	if err != nil {
		return err
	}

	termlog.Flush()
	return nil
}

func (c *Client) isReady() bool {
	if atomic.LoadUint32(&c.ready) == 1 {
		return true
	}

	if atomic.LoadUint32(&c.resourcesReady) == 1 {
		atomic.StoreUint32(&c.ready, 1)
		return true
	}

	return false
}

func (c *Client) loadResources() {
	err := c.resources.LoadAll()

	if err != nil {
		panic(err)
	}

	atomic.StoreUint32(&c.resourcesReady, 1)
}

//func (*Boot) Setup(u engo.Updater) {
//	w, _ := u.(*ecs.World)
//
//	common.SetBackground(color.White)
//
//	w.AddSystem(&common.RenderSystem{})
//	w.AddSystem(&common.AnimationSystem{})
//	w.AddSystem(&common.MouseSystem{})
//	w.AddSystem(&SpawnSystem{w: w})
//	w.AddSystem(NewFlySystem(w))
//	w.AddSystem(NewTilemapSystem(w, "map.tmx"))
//}
