package bench_test

import (
	"testing"

	"insmo.com/play/data"
)

type Component struct {
	data.SpaceComponent
	data.HealthComponent
	Pos    float32
	Sprite uint8
}

func BenchmarkPassByValue(b *testing.B) {
	b.StopTimer()
	CN := 100000
	entities := make([]Component, 0, CN)

	for i := 0; i < CN; i++ {
		entities = append(entities, Component{})
	}
	M := len(entities)
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		entities[i%M].Health = i + 1
	}

	b.StopTimer()
	if entities[0].Health <= 0 {
		b.Fatal("Health should not be zero")
	}
}

func BenchmarkPassByReference(b *testing.B) {
	b.StopTimer()
	CN := 100000
	entities := make([]*Component, 0, CN)

	for i := 0; i < CN; i++ {
		entities = append(entities, &Component{})
	}
	M := len(entities)
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		entities[i%M].Health = i + 1
	}

	b.StopTimer()
	if entities[0].Health <= 0 {
		b.Fatal("Health should not be zero")
	}
}

func BenchmarkPassByValueRange(b *testing.B) {
	b.StopTimer()
	CN := 10000
	entities := make([]Component, 0, CN)

	for i := 0; i < CN; i++ {
		entities = append(entities, Component{})
	}
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		for j := 0; j < CN; j++ {
			entities[j].Health = j + 1
		}
	}

	b.StopTimer()
	if entities[0].Health <= 0 {
		b.Fatal("Health should not be zero")
	}
}

func BenchmarkPassByReferenceRange(b *testing.B) {
	b.StopTimer()
	CN := 10000
	entities := make([]*Component, 0, CN)

	for i := 0; i < CN; i++ {
		entities = append(entities, &Component{})
	}
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		for j, ent := range entities {
			ent.Health = j + 1
		}
	}

	b.StopTimer()
	if entities[0].Health <= 0 {
		b.Fatal("Health should not be zero")
	}
}
