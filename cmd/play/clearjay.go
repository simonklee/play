// +build !js

package main

import (
	"fmt"
	"os"

	"github.com/alecthomas/kingpin"

	"insmo.com/play"
	"insmo.com/play/client"
)

func fatal(err error) {
	fmt.Fprintf(os.Stderr, "%v", err)
	os.Exit(1)
}

func main() {
	kingpin.Version(Version())
	kingpin.Parse()

	client := client.NewClient()
	err := client.Run()

	if err != nil && err != play.ErrQuit {
		fatal(err)
	}
}

var githash, gittag, buildstamp, goversion string

func Version() string {
	if gittag != "" {
		return fmt.Sprintf("gittag: %s githash: %s built: %s with %s", gittag, githash, buildstamp, goversion)
	}
	return fmt.Sprintf("githash: %s built: %s with %s", githash, buildstamp, goversion)
}
