// +build js

package main

import (
	"insmo.com/play/client"
)

func main() {
	client := client.NewClient()
	client.Run()
}

var githash, gittag, buildstamp, goversion string
