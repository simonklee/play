package types

const (
	maxGenBits uint32 = 6
	maxTypBits uint32 = 8
	genMask    uint32 = -1 ^ (-1 << 6)
	typMask    uint32 = -1 ^ (-1 << 8)
	indexMask  uint32 = -1 ^ (-1 << 18)
)

// ComponentType represents a unique component type.
type ComponentType uint8

// ComponentID is a distinct thing. It is represented as a unique uint32.
type ComponentID uint32

// newComponentID assembles a ComponentID by packing the index, type and
// generation.
func NewComponentID(idx uint32, gen uint8, typ uint8) ComponentID {
	if idx > indexMask {
		panic("idx overflow")
	}

	return ComponentID((idx << (maxTypBits + maxGenBits)) | (uint32(typ) << maxGenBits) | uint32(gen))
}

// Index returns the index value for given ComponentID (internal).
func (c ComponentID) Index() uint32 {
	return (uint32(c) >> (maxTypBits + maxGenBits)) & indexMask
}

// Type returns the type for given ComponentID.
func (c ComponentID) Type() ComponentType {
	return ComponentType((uint32(c) >> maxGenBits) & genMask)
}

// Generation returns the generation for given ComponentID (internal).
func (c ComponentID) Generation() uint8 {
	return uint8(uint32(c) & genMask)
}

// Components is an interface implemented for each collection of Component
// types. If you create a Position component, you need to create a []Position
// type which implements the Components interface.
type Components interface {
	Swap(i, j int)
	Len() int
	PopBack()
	Add(interface{})
}

type Sortable interface {
	Swap(i, j int)
	Len() int
	Less(i, j int) bool
}
