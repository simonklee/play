package types

const (
	entityMaxGenBits uint32 = 8
	entityGenMask    uint32 = -1 ^ (-1 << 8)
	entityIndexMask  uint32 = -1 ^ (-1 << 24)
)

// Entity is a distinct thing. It is represented as a unique uint32.
type Entity uint32

// Index returns the index value for given Entity (internal).
func (e Entity) Index() uint32 {
	return (uint32(e) >> entityMaxGenBits) & entityIndexMask
}

// Generation returns the generation for given Entity (internal).
func (e Entity) Generation() uint8 {
	return uint8(uint32(e) & entityGenMask)
}

// NewEntity assembles an entity by packing the index and generation
func NewEntity(idx uint32, gen uint8) Entity {
	if idx > entityIndexMask {
		panic("idx overflow")
	}

	return Entity((idx << entityMaxGenBits) | uint32(gen))
}
