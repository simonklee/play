package use

import (
	"fmt"

	"github.com/EngoEngine/math"
)

type Vec struct {
	X, Y float32
}

var ZeroVec Vec

func V(x, y float32) Vec {
	return Vec{
		X: x,
		Y: y,
	}
}

func V64(x, y float64) Vec {
	return Vec{
		X: float32(x),
		Y: float32(y),
	}
}

// Add returns the sum of vectors u and v.
func (u Vec) F64() (float64, float64) {
	return float64(u.X), float64(u.Y)
}

// Angle returns the angle between the vector u and the x-axis. The result is in range [-Pi, Pi].
func (u Vec) Angle() float32 {
	return math.Atan2(u.Y, u.X)
}

// Scaled returns the vector u multiplied by c.
func (u Vec) Scaled(p float32) Vec {
	return Vec{
		X: u.X * p,
		Y: u.Y * p,
	}
}

// Add returns the sum of vectors u and v.
// Add returns the vector p+q.
func (p Vec) Add(q Vec) Vec {
	return Vec{
		p.X + q.X,
		p.Y + q.Y,
	}
}

// Sub returns the vector p-q.
func (p Vec) Sub(q Vec) Vec {
	return Vec{p.X - q.X, p.Y - q.Y}
}

// Mul returns the vector p*k.
func (p Vec) Mul(k float32) Vec {
	return Vec{p.X * k, p.Y * k}
}

// Div returns the vector p/k.
func (p Vec) Div(k float32) Vec {
	return Vec{p.X / k, p.Y / k}
}

// Rotated returns the vector u rotated by the given angle in radians.
func (u Vec) Rotated(angle float32) Vec {
	sin, cos := math.Sincos(angle)
	return Vec{
		u.X*cos - u.Y*sin,
		u.X*sin + u.Y*cos,
	}
}

func (u Vec) String() string {
	return fmt.Sprintf("(%v:%v)", u.X, u.Y)
}

type Rect struct {
	Min, Max Vec
}

var ZeroRect Rect

func R(x1, y1, x2, y2 float32) Rect {
	if x1 > x2 {
		x1, x2 = x2, x1
	}
	if y1 > y2 {
		y1, y2 = y2, y1
	}
	return Rect{
		Min: V(x1, y1),
		Max: V(x2, y2),
	}
}

func (r Rect) W() float32 {
	return r.Max.X - r.Min.X
}

func (r Rect) H() float32 {
	return r.Max.Y - r.Min.Y
}

func (r Rect) Center() Vec {
	return V(
		r.Min.X+r.W()/2,
		r.Min.Y+r.H()/2,
	)
}

func (r Rect) ScaledAtCenter(factor float32) Rect {
	w := r.W()
	h := r.H()
	c := r.Center()
	return R(
		c.X-factor*w/2,
		c.Y-factor*h/2,
		c.X+factor*w/2,
		c.Y+factor*h/2,
	)
}

// Intersect returns the largest rectangle contained by both r and s. If the
// two rectangles do not overlap then the zero rectangle will be returned.
func (r Rect) Intersect(s Rect) Rect {
	if r.Min.X < s.Min.X {
		r.Min.X = s.Min.X
	}
	if r.Min.Y < s.Min.Y {
		r.Min.Y = s.Min.Y
	}
	if r.Max.X > s.Max.X {
		r.Max.X = s.Max.X
	}
	if r.Max.Y > s.Max.Y {
		r.Max.Y = s.Max.Y
	}
	// Letting r0 and s0 be the values of r and s at the time that the method
	// is called, this next line is equivalent to:
	//
	// if max(r0.Min.X, s0.Min.X) >= min(r0.Max.X, s0.Max.X) || likewiseForY { etc }
	if r.Empty() {
		return ZeroRect
	}
	return r
}

// Union returns the smallest rectangle that contains both r and s.
func (r Rect) Union(s Rect) Rect {
	if r.Empty() {
		return s
	}
	if s.Empty() {
		return r
	}
	if r.Min.X > s.Min.X {
		r.Min.X = s.Min.X
	}
	if r.Min.Y > s.Min.Y {
		r.Min.Y = s.Min.Y
	}
	if r.Max.X < s.Max.X {
		r.Max.X = s.Max.X
	}
	if r.Max.Y < s.Max.Y {
		r.Max.Y = s.Max.Y
	}
	return r
}

// Empty reports whether the rectangle contains no points.
func (r Rect) Empty() bool {
	return r.Min.X >= r.Max.X || r.Min.Y >= r.Max.Y
}

// Eq reports whether r and s contain the same set of points. All empty
// rectangles are considered equal.
func (r Rect) Eq(s Rect) bool {
	return r == s || r.Empty() && s.Empty()
}

// Overlaps reports whether r and s have a non-empty intersection.
func (r Rect) Overlaps(s Rect) bool {
	return !r.Empty() && !s.Empty() &&
		r.Min.X < s.Max.X && s.Min.X < r.Max.X &&
		r.Min.Y < s.Max.Y && s.Min.Y < r.Max.Y
}

// In reports whether every point in r is in s.
func (r Rect) In(s Rect) bool {
	if r.Empty() {
		return true
	}
	// Note that r.Max is an exclusive bound for r, so that r.In(s)
	// does not require that r.Max.In(s).
	return s.Min.X <= r.Min.X && r.Max.X <= s.Max.X &&
		s.Min.Y <= r.Min.Y && r.Max.Y <= s.Max.Y
}

// Canon returns the canonical version of r. The returned rectangle has minimum
// and maximum coordinates swapped if necessary so that it is well-formed.
func (r Rect) Canon() Rect {
	if r.Max.X < r.Min.X {
		r.Min.X, r.Max.X = r.Max.X, r.Min.X
	}
	if r.Max.Y < r.Min.Y {
		r.Min.Y, r.Max.Y = r.Max.Y, r.Min.Y
	}
	return r
}

func (r Rect) String() string {
	return fmt.Sprintf("%s, %s", r.Min, r.Max)
}
