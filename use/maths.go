package use

import (
	"github.com/EngoEngine/math"
	"github.com/hajimehoshi/ebiten"
)

// DegToRad converts degrees to radians
func DegToRad(deg float32) (rad float32) {
	return deg * math.Pi / 180
}

// RadToDeg converts radians to degrees
func RadToDeg(rad float32) (deg float32) {
	return rad * 180 / math.Pi
}

// Fit returns the Matrix that will transform a source Rect
// into the dest Rect
func Fit(source, dest Rect) ebiten.GeoM {
	scaleX := float64(dest.W() / source.W())
	scaleY := float64(dest.H() / source.H())

	mat := ebiten.GeoM{}
	mat.Translate(float64(-source.Min.X), float64(-source.Min.Y))
	mat.Scale(scaleX, scaleY)
	mat.Translate(float64(dest.Min.X), float64(dest.Min.Y))

	return mat
}

// FitGeoM returns the Matrix that will transform a source Rect
// into the dest Rect
func FitRotated(rot float32, source, dest Rect) ebiten.GeoM {
	scaleX := float64(dest.W() / source.W())
	scaleY := float64(dest.H() / source.H())

	mat := ebiten.GeoM{}

	// rotate about center of source
	mat.Translate(float64(-source.W()/2), float64(-source.W()/2))
	mat.Rotate(float64(rot))
	mat.Translate(float64(source.W()/2), float64(source.W()/2))

	// scale
	mat.Scale(scaleX, scaleY)

	// move to destination
	mat.Translate(float64(dest.Min.X), float64(dest.Min.Y))

	return mat
}

// Collision returns if two rectangles intersect
func Collision(r1, r2 Rect) bool {
	if r1.Min.X > r2.Max.X || r2.Min.X > r1.Max.X {
		return false
	}
	if r1.Min.Y > r2.Max.Y || r2.Min.Y > r1.Max.Y {
		return false
	}
	return true
}
