package use

import (
	"fmt"
)

type IntVec struct {
	X, Y int32
}

var ZeroIntVec IntVec

func IntV(x, y int32) IntVec {
	return IntVec{
		X: x,
		Y: y,
	}
}

func IntV64(x, y int) IntVec {
	return IntVec{
		X: int32(x),
		Y: int32(y),
	}
}

// Add returns the sum of vectors u and v.
func (u IntVec) Int() (int, int) {
	return int(u.X), int(u.Y)
}

// Scaled returns the vector u multiplied by c.
func (u IntVec) Scaled(p int32) IntVec {
	return IntVec{
		X: u.X * p,
		Y: u.Y * p,
	}
}

// Add returns the vector p+q.
func (p IntVec) Add(q IntVec) IntVec {
	return IntVec{
		p.X + q.X,
		p.Y + q.Y,
	}
}

// Sub returns the vector p-q.
func (p IntVec) Sub(q IntVec) IntVec {
	return IntVec{p.X - q.X, p.Y - q.Y}
}

// Mul returns the vector p*k.
func (p IntVec) Mul(k int32) IntVec {
	return IntVec{p.X * k, p.Y * k}
}

// Div returns the vector p/k.
func (p IntVec) Div(k int32) IntVec {
	return IntVec{p.X / k, p.Y / k}
}

func (u IntVec) String() string {
	return fmt.Sprintf("(%v:%v)", u.X, u.Y)
}

type IntRect struct {
	Min, Max IntVec
}

var ZeroIntRect IntRect

func IntR(x1, y1, x2, y2 int32) IntRect {
	if x1 > x2 {
		x1, x2 = x2, x1
	}
	if y1 > y2 {
		y1, y2 = y2, y1
	}
	return IntRect{
		Min: IntV(x1, y1),
		Max: IntV(x2, y2),
	}
}

func (r IntRect) W() int32 {
	return r.Max.X - r.Min.X
}

func (r IntRect) H() int32 {
	return r.Max.Y - r.Min.Y
}

func (r IntRect) Center() IntVec {
	return IntV(
		r.Min.X+r.W()/2,
		r.Min.Y+r.H()/2,
	)
}

func (r IntRect) ScaledAtCenter(factor int32) IntRect {
	w := r.W()
	h := r.H()
	c := r.Center()
	return IntR(
		c.X-factor*w/2,
		c.Y-factor*h/2,
		c.X+factor*w/2,
		c.Y+factor*h/2,
	)
}

// Intersect returns the largest rectangle contained by both r and s. If the
// two rectangles do not overlap then the zero rectangle will be returned.
func (r IntRect) Intersect(s IntRect) IntRect {
	if r.Min.X < s.Min.X {
		r.Min.X = s.Min.X
	}
	if r.Min.Y < s.Min.Y {
		r.Min.Y = s.Min.Y
	}
	if r.Max.X > s.Max.X {
		r.Max.X = s.Max.X
	}
	if r.Max.Y > s.Max.Y {
		r.Max.Y = s.Max.Y
	}
	// Letting r0 and s0 be the values of r and s at the time that the method
	// is called, this next line is equivalent to:
	//
	// if max(r0.Min.X, s0.Min.X) >= min(r0.Max.X, s0.Max.X) || likewiseForY { etc }
	if r.Empty() {
		return ZeroIntRect
	}
	return r
}

// Union returns the smallest rectangle that contains both r and s.
func (r IntRect) Union(s IntRect) IntRect {
	if r.Empty() {
		return s
	}
	if s.Empty() {
		return r
	}
	if r.Min.X > s.Min.X {
		r.Min.X = s.Min.X
	}
	if r.Min.Y > s.Min.Y {
		r.Min.Y = s.Min.Y
	}
	if r.Max.X < s.Max.X {
		r.Max.X = s.Max.X
	}
	if r.Max.Y < s.Max.Y {
		r.Max.Y = s.Max.Y
	}
	return r
}

// Empty reports whether the rectangle contains no points.
func (r IntRect) Empty() bool {
	return r.Min.X >= r.Max.X || r.Min.Y >= r.Max.Y
}

// Eq reports whether r and s contain the same set of points. All empty
// rectangles are considered equal.
func (r IntRect) Eq(s IntRect) bool {
	return r == s || r.Empty() && s.Empty()
}

// Overlaps reports whether r and s have a non-empty intersection.
func (r IntRect) Overlaps(s IntRect) bool {
	return !r.Empty() && !s.Empty() &&
		r.Min.X < s.Max.X && s.Min.X < r.Max.X &&
		r.Min.Y < s.Max.Y && s.Min.Y < r.Max.Y
}

// In reports whether every point in r is in s.
func (r IntRect) In(s IntRect) bool {
	if r.Empty() {
		return true
	}
	// Note that r.Max is an exclusive bound for r, so that r.In(s)
	// does not require that r.Max.In(s).
	return s.Min.X <= r.Min.X && r.Max.X <= s.Max.X &&
		s.Min.Y <= r.Min.Y && r.Max.Y <= s.Max.Y
}

// Canon returns the canonical version of r. The returned rectangle has minimum
// and maximum coordinates swapped if necessary so that it is well-formed.
func (r IntRect) Canon() IntRect {
	if r.Max.X < r.Min.X {
		r.Min.X, r.Max.X = r.Max.X, r.Min.X
	}
	if r.Max.Y < r.Min.Y {
		r.Min.Y, r.Max.Y = r.Max.Y, r.Min.Y
	}
	return r
}

func (r IntRect) String() string {
	return fmt.Sprintf("%s, %s", r.Min, r.Max)
}
