package data

import "insmo.com/play/data/sprite"

type Player struct {
	SpaceComponent
	HealthComponent
	sprite.SpriteName
	sprite.AnimationName
	Name     string
	LastFlip float32
}
