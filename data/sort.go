package data

import "insmo.com/play/types"

// Less reports whether the element with
// index i should sort before the element with index j.
// TODO: Kind of dummy thing to test this.
func (d *SpaceComponents) Less(i, j int) bool {
	return (*d)[i].Position.X < (*d)[j].Position.X
}

// Test that SpaceComponents implements the types.Sortable interface
var _ types.Sortable = &SpaceComponents{}
