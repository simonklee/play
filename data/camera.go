package data

import (
	"insmo.com/play/types"
	"insmo.com/play/use"
)

type Camera struct {
	Tracking    types.Entity
	WorldPos    use.Vec
	WorldBounds use.Rect
	Width       float32
	Height      float32
	Scale       float32
}

func (c Camera) ScreenToWorld(q use.Vec) use.Vec {
	return c.WorldPos.Add(q)
}

func (c Camera) WorldToScreen(q use.Vec) use.Vec {
	return q.Sub(c.WorldPos)
}
