package data

import "insmo.com/play/use"

type SpaceComponent struct {
	Position use.Vec
	Width    float32
	Height   float32
}

//func ToSpaceComponent(c Component) *SpaceComponent {
//	return (*SpaceComponent)(c)
//}

type WaterComponent struct {
	Amount      int
	Temperature int8
}

type SoilType uint8

const (
	SandSoilType SoilType = iota + 1
	SiltSoilType
	ClaySoilType
	LoamSoilType
)

type Soil struct {
	WaterComponent
	Type SoilType
}

type HealthComponent struct {
	Health int
}
