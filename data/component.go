package data

import (
	"insmo.com/play/types"
)

//type Component unsafe.Pointer

type Component struct {
	Type      types.ComponentType
	Component interface{}
}

type EntityComponent struct {
	Entity types.Entity
	// possible to optimize type conversion by making component an
	// unsafe.Pointer, but and when we assign a component to an entity we need
	// to defer it using reflect.ValueOf. Tradeoffs to consider later.
	// v := reflect.ValueOf(c)
	// ty := unsafe.Pointer(v.Pointer())
	// Component unsafe.Pointer
	Type      types.ComponentType
	Component interface{}
}

type EntityComponentUnion struct {
	A EntityComponent
	B EntityComponent
}

//const (
//	RenderComponentType ComponentType = 1 << iota
//	SpaceComponentType
//	SoilComponentType
//	WaterComponentType
//	HealthComponentType
//)
//
//type ComponentMask uint64
//
//func (f ComponentMask) Has(flag ComponentType) bool {
//	return f&ComponentMask(flag) != 0
//}
//
//func (f ComponentMask) Set(flag ComponentType) ComponentMask {
//	return f | ComponentMask(flag)
//}
//
//func (f ComponentMask) Clear(flag ComponentType) ComponentMask {
//	return f &^ ComponentMask(flag)
//}
//
//func (f ComponentMask) Toggle(flag ComponentType) ComponentMask {
//	return f ^ ComponentMask(flag)
//}
