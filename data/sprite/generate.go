package sprite

//go:generate go run gensprite.go
//go:generate stringer -type=SpriteName,AnimationName -output sprite_string.go sprite.go
//go:generate gofmt -s -w .
