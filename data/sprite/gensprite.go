// +build ignore

package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"text/template"
	"unicode"
	"unsafe"

	"insmo.com/play/resource"
	"insmo.com/play/use"
)

const (
	AsePlayForward  = "forward"
	AsePlayReverse  = "reverse"
	AsePlayPingPong = "pingpong"
)

type AseFrame struct {
	Frame struct {
		X int `json:"x"`
		Y int `json:"y"`
		W int `json:"w"`
		H int `json:"h"`
	} `json:"frame"`
	Rotated          bool `json:"rotated"`
	Trimmed          bool `json:"trimmed"`
	SpriteSourceSize struct {
		X int `json:"x"`
		Y int `json:"y"`
		W int `json:"w"`
		H int `json:"h"`
	} `json:"spriteSourceSize"`
	SourceSize struct {
		W int `json:"w"`
		H int `json:"h"`
	} `json:"sourceSize"`
	Duration int `json:"duration"` // convert ms -> sec (float32)
	//float32(Duration) / 1000
}

type AseMeta struct {
	App     string `json:"app"`
	Version string `json:"version"`
	Image   string `json:"image"`
	Format  string `json:"format"`
	Size    struct {
		W int `json:"w"`
		H int `json:"h"`
	} `json:"size"`
	Scale     string `json:"scale"`
	FrameTags []struct {
		Name      string `json:"name"`
		From      int    `json:"from"`
		To        int    `json:"to"`
		Direction string `json:"direction"`
	} `json:"frameTags"`
}

type Sprite struct {
	Identifier string
	View       use.IntRect
	Resource   resource.Resource
}

type Frame struct {
	Sprite   Sprite
	Duration float32
}

type Animation struct {
	Frames     []Frame
	Identifier string
	Duration   float32
}

type OutSprite struct {
	View       use.IntRect
	Identifier uint8
	Resource   resource.Resource
}

type OutFrame struct {
	Sprite   OutSprite
	Duration float32
}

type OutFrame2 struct {
	View       use.IntRect
	Identifier uint8
	Resource   resource.Resource
	Duration   float32
}

type OutFrame3 struct {
	Sprite   *OutSprite
	Duration float32
}

type OutAnimation struct {
	Frames     []OutFrame
	Duration   float32
	Identifier uint16
}

type Aseprite struct {
	Frames map[string]AseFrame `json:"frames"`
	Meta   AseMeta             `json:"meta"`
}

func ParseAseprite(r io.Reader) (*Aseprite, error) {
	dec := json.NewDecoder(r)
	ase := Aseprite{}
	err := dec.Decode(&ase)

	if err != nil {
		return nil, err
	}

	return &ase, nil
}

func toRec(x, y, w, h int) use.IntRect {
	return use.IntR(
		int32(x),
		int32(y),
		int32(x)+int32(w),
		int32(y)+int32(h))
}

func msToSec(ms int) float32 {
	return float32(ms) / 1000
}

var knownFuncs = make(map[string]int)
var regName = regexp.MustCompile(`[^a-zA-Z0-9_]`)

// safeName converts the given name into a name
// which qualifies as a valid identifier. It
// also compares against a known list of names to
// prevent conflict based on name translation.
func safeName(name string, knownFuncs map[string]int) string {
	var inBytes, outBytes []byte
	var toUpper bool

	name = strings.ToLower(name)
	inBytes = []byte(name)

	for i := 0; i < len(inBytes); i++ {
		if regName.Match([]byte{inBytes[i]}) {
			toUpper = true
		} else if toUpper {
			outBytes = append(outBytes, bytes.ToUpper([]byte{inBytes[i]})...)
			toUpper = false
		} else {
			outBytes = append(outBytes, inBytes[i])
		}
	}

	outBytes[0] = bytes.ToUpper([]byte{inBytes[0]})[0]
	name = string(outBytes)

	// Identifier can't start with a digit.
	if unicode.IsDigit(rune(name[0])) {
		name = "Z" + name
	}

	if num, ok := knownFuncs[name]; ok {
		knownFuncs[name] = num + 1
		name = fmt.Sprintf("%s%d", name, num)
	} else {
		knownFuncs[name] = 2
	}

	return name
}

var regFrameNumber = regexp.MustCompile(`[0-9]$`)

func spriteIdentifier(key string) string {
	// Remove extension
	ext := path.Ext(key)
	name := key[:len(key)-len(ext)]
	num := regFrameNumber.FindString(name)

	// this is if there was no frame digit identifier
	if num == "" {
		return safeName(name+" Frame0", knownFuncs)
	}

	_, err := strconv.Atoi(num)

	if err != nil {
		log.Fatalf("cannot find frame number for %s - %s", key, name)
	}

	return safeName(name[:len(name)-len(num)]+"Frame"+num, knownFuncs)
}

func animationIdentifier(key string) string {
	// Remove extension
	//ext := path.Ext(key)
	//name := key[:len(key)-(len(ext)+1)]
	name := key
	return safeName(name, knownFuncs)
}

var regEndsWithFrameZero = regexp.MustCompile(`Frame0$`)

func isNewfile(prev, key string) bool {
	if prev == key {
		return false
	}

	return regEndsWithFrameZero.MatchString(key)
}

// findFrameIndex finds the longest prefix of name in filemap
func findFrameIndex(filemap map[string]int, name string) (string, bool) {
	longestMatch := ""
	longestMatchId := ""

	for id := range filemap {
		current := ""

		for j := len(name); j > 0; j-- {
			prefix := name[:j]

			if strings.HasPrefix(id, prefix) {
				current = prefix
				break
			}
		}

		if len(current) > len(longestMatch) {
			longestMatch = current
			longestMatchId = id
		}
	}

	if longestMatchId == "" {
		return "", false
	}

	return longestMatchId, true
}

var tmplConsts = template.Must(template.New("sprite").Parse(`
// Code generated by gensprite.go using 'go generate'. DO NOT EDIT

package sprite

type SpriteName {{$.SpriteType}}

const (
	{{range $i, $frame := .Frames}}{{$frame.Sprite.Identifier}} SpriteName = {{$i}}
	{{end}}
)

type AnimationName {{$.AnimationType}}

const (
	{{range $i, $anim := .Animations}}{{$anim.Identifier}} AnimationName = {{$i}}
	{{end}}
)
`))

var tmpl = template.Must(template.New("sprite").Parse(`
// Code generated by gensprite.go using 'go generate'. DO NOT EDIT

package sprite

import (
	"insmo.com/play/resource"
	"insmo.com/play/use"
)

type Sprite struct {
	View       use.IntRect
	Resource   resource.Resource // 1 byte
	Identifier SpriteName // not really necessary, but nice for debugging
}

type Frame struct {
	Sprite   Sprite
	Duration float32
}

type Animation struct {
	Frames     []Frame // 24 bytes (SliceHeader) + Frame*N
	Duration   float32 // 4 bytes
	Identifier AnimationName // 1-8 bytes
}

var sprites = []Sprite{
{{range $sprite := .Sprites}}{
	Identifier: {{$sprite.Identifier}},
	View:       use.IntR({{$sprite.View.Min.X}}, {{$sprite.View.Min.Y}}, {{$sprite.View.Max.X}}, {{$sprite.View.Max.Y}}),
	Resource:   resource.ImagesSpriteAtlasPng,
},
{{end}}
}

func GetSprite(id SpriteName) Sprite {
	return sprites[id]
}

var frames = []Frame{
{{range $i, $frame := .Frames}}{
	Sprite: sprites[{{$i}}],
	Duration: {{$frame.Duration}},
},
{{end}}
}

func getFrame(id SpriteName) Frame {
	return frames[id]
}

var animations = []*Animation{
{{range $anim := .Animations}}{
	Frames: []Frame{
{{range $frame := $anim.Frames}}getFrame({{$frame.Sprite.Identifier}}),
{{end}}
},
	Identifier: {{$anim.Identifier}},
	Duration: {{$anim.Duration}},
},
{{end}}
}

func GetAnimation(id AnimationName) *Animation {
	return animations[id]
}
`))

//var resourceToPath = map[Resource]string{
//	{{range $res := .Resources}}{{$res.Name}}: "{{$res.Path}}",
//	{{end}}
//}
//
//var pathToResurce = map[string]Resource{
//	{{range $res := .Resources}}"{{$res.Path}}": {{$res.Name}},
//	{{end}}
//}
//
//var imageResources = []Resource{
//	{{range $res := .Images}}{{$res.Name}},
//	{{end}}
//}
//`))

func (ase *Aseprite) Translate() (*Context, error) {
	if len(ase.Frames) == 0 {
		return nil, fmt.Errorf("no frames")
	}

	keys := []string{}

	for key := range ase.Frames {
		keys = append(keys, key)
	}

	sort.Strings(keys)
	// frames is a slice of all frames
	frames := make([]Frame, len(keys))
	sprites := make([]Sprite, len(keys))
	// a lookup table which maps filenames to index in frames array
	filemap := make(map[string]int)
	current := ""

	for i, key := range keys {
		af := ase.Frames[key]
		id := spriteIdentifier(key)

		sprites[i] = Sprite{
			Resource:   resource.ImagesSpriteAtlasPng,
			Identifier: id,
			View:       toRec(af.Frame.X, af.Frame.Y, af.Frame.W, af.Frame.H),
		}

		frames[i] = Frame{
			Sprite:   sprites[i],
			Duration: msToSec(af.Duration),
		}

		if isNewfile(current, id) {
			filemap[id] = i
			current = id
		}
	}

	// animations is a slice of all animations
	animations := make([]Animation, len(ase.Meta.FrameTags))

	for i, tag := range ase.Meta.FrameTags {
		id := animationIdentifier(tag.Name)
		fileId, ok := findFrameIndex(filemap, id)

		if !ok {
			return nil, fmt.Errorf("cannot find frameIndex for animation id %s", id)
		}

		frameIdx := filemap[fileId]
		animFrames := frames[frameIdx+tag.From : frameIdx+tag.To+1]
		var duration float32

		for _, f := range animFrames {
			duration += f.Duration
		}

		animations[i] = Animation{
			Identifier: id,
			Frames:     animFrames,
			Duration:   duration,
		}
	}

	return &Context{
		Frames:     frames,
		Animations: animations,
		Sprites:    sprites,
	}, nil
}

type Context struct {
	Frames     []Frame
	Animations []Animation
	Sprites    []Sprite
}

func minUintTypeString(v int) string {
	if v < int(^uint8(0)) {
		return "uint8"
	} else if v < int(^uint16(0)) {
		return "uint16"
	} else if v < int(^uint32(0)) {
		return "uint32"
	}

	return "uint64"
}

func (c *Context) TemplateConsts(w io.Writer) error {
	return tmplConsts.Execute(w, map[string]interface{}{
		"AnimationType": minUintTypeString(len(c.Animations)),
		"SpriteType":    minUintTypeString(len(c.Sprites)),
		"Sprites":       c.Sprites,
		"Frames":        c.Frames,
		"Animations":    c.Animations,
	})
}

func (c *Context) Template(w io.Writer) error {
	return tmpl.Execute(w, map[string]interface{}{
		"AnimationType": minUintTypeString(len(c.Animations)),
		"SpriteType":    minUintTypeString(len(c.Sprites)),
		"Sprites":       c.Sprites,
		"Frames":        c.Frames,
		"Animations":    c.Animations,
	})
}

func (c *Context) Debug() {
	outFrames := []OutFrame{}
	outAnimation := OutAnimation{}
	outAnimation10 := OutAnimation{Frames: make([]OutFrame, 0, 10)}
	os := OutSprite{}
	of := OutFrame{Sprite: os}
	fmt.Printf(`
SizeOf Animations:            %d bytes
SizeOf Frames:                %d bytes
SizeOf Frame:                 %d bytes
SizeOf Frame.Sprite:          %d bytes
SizeOf Frame.Duration:        %d bytes
SizeOf OutFrames:             %d bytes
SizeOf OutFrames.Frame:       %d bytes
SizeOf OutFrames.Sprite:      %d bytes
SizeOf OutFrames.Duration:    %d bytes
---
SizeOf OutSprite:             %d bytes
SizeOf OutFrame:              %d bytes
SizeOf OutFrame2:             %d bytes
SizeOf OutFrame3:             %d bytes
SizeOf OutAnimation:          %d bytes
SizeOf OutAnimation.Frames:   %d bytes
SizeOf OutAnimation10:        %d bytes
SizeOf OutAnimation10.Frames: %d bytes
---
SizeOf IntRect:               %d bytes
SizeOf Resource:              %d bytes
----------
--- Memory
Addr OutSprite:               %p ptr addr
Addr OutFrame:                %p ptr addr
Addr OutFrame.Sprite:         %p ptr addr
`,
		unsafe.Sizeof(c.Animations),
		unsafe.Sizeof(c.Frames),
		unsafe.Sizeof(c.Frames[0]),
		unsafe.Sizeof(c.Frames[0].Sprite),
		unsafe.Sizeof(c.Frames[0].Duration),
		unsafe.Sizeof(outFrames),
		unsafe.Sizeof(outFrames[0]),
		unsafe.Sizeof(outFrames[0].Sprite),
		unsafe.Sizeof(outFrames[0].Duration),
		unsafe.Sizeof(OutSprite{}),
		unsafe.Sizeof(OutFrame{}),
		unsafe.Sizeof(OutFrame2{}),
		unsafe.Sizeof(OutFrame3{}),
		unsafe.Sizeof(outAnimation),
		unsafe.Sizeof(outAnimation.Frames),
		unsafe.Sizeof(outAnimation10),
		unsafe.Sizeof(outAnimation10.Frames),
		unsafe.Sizeof(use.IntRect{}),
		unsafe.Sizeof(resource.Resource(0)),
		&os,
		&of,
		&(of.Sprite),
	)
}

var atlasFile = flag.String("atlas", "../../assets/atlas/atlas.json", "path to atlas.json filename")

func main() {
	flag.Parse()

	fp, err := os.Open(*atlasFile)

	if err != nil {
		log.Fatal(err)
	}

	defer fp.Close()
	ase, err := ParseAseprite(fp)

	if err != nil {
		log.Fatal(err)
	}

	c, err := ase.Translate()

	if err != nil {
		log.Fatal(err)
	}

	f, err := os.Create("sprite.go")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	err = c.TemplateConsts(f)

	if err != nil {
		log.Fatal(err)
	}

	f, err = os.Create("lookup.go")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	err = c.Template(f)

	if err != nil {
		log.Fatal(err)
	}

	c.Debug()
}
