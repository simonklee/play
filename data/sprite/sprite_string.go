// Code generated by "stringer -type=SpriteName,AnimationName -output sprite_string.go sprite.go"; DO NOT EDIT.

package sprite

import "strconv"

const _SpriteName_name = "BluebirdFrame0BluebirdFrame1BluebirdFrame2BluebirdFrame3BrickFrame0PidgonFrame0PidgonFrame1PidgonFrame2PidgonFrame3"

var _SpriteName_index = [...]uint8{0, 14, 28, 42, 56, 67, 79, 91, 103, 115}

func (i SpriteName) String() string {
	if i >= SpriteName(len(_SpriteName_index)-1) {
		return "SpriteName(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _SpriteName_name[_SpriteName_index[i]:_SpriteName_index[i+1]]
}

const _AnimationName_name = "BlubirdFlyingPidgonIdlePidgonIdling"

var _AnimationName_index = [...]uint8{0, 13, 23, 35}

func (i AnimationName) String() string {
	if i >= AnimationName(len(_AnimationName_index)-1) {
		return "AnimationName(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _AnimationName_name[_AnimationName_index[i]:_AnimationName_index[i+1]]
}
