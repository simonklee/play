package data

//go:generate go run gentypes.go
//go:generate gofmt -s -w .
//
// Skip, doesn't work because ComponentType is externally defined in `types`
// Skip go:generate stringer -type=ComponentType types.go
