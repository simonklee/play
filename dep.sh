#!/usr/bin/env bash

set -x
set -e

go get -v -u github.com/golang/dep
dep ensure -update
dep prune
