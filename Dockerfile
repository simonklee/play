FROM golang:1.10.1-stretch

LABEL maintainer "kogama.com <simon@insmo.com>"

RUN set -ex; \
    apt-get update -qy; \
    \
    apt-get install -y \
        -o APT::Install-Recommends=false \
        -o APT::Install-Suggests=false \
        libglu1-mesa-dev \
        libgles2-mesa-dev \
        libxrandr-dev \
        libxcursor-dev \
        libxinerama-dev \
        libxi-dev \
        libasound2-dev \
        libglew-dev \
        libalut-dev \
        xvfb \
        libxxf86vm-dev; \
    \
    rm -rf /var/lib/apt/lists/*; \
    \
    go get \
        github.com/alecthomas/kingpin \
        github.com/buger/goterm \
        github.com/EngoEngine/math \
        github.com/go-gl/mathgl/mgl32 \
        github.com/hajimehoshi/ebiten \
        github.com/stretchr/testify
