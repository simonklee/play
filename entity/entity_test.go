package entity_test

import (
	"fmt"
	"testing"

	"insmo.com/play/entity"
	"insmo.com/play/types"
)

func TestEntity(t *testing.T) {
	m := entity.NewManager()
	seen := make(map[types.Entity]int, 4048)

	for i := 0; i <= 1100; i++ {
		e := m.Create()
		seen[e]++

		if seen[e] > 1 {
			t.Fatalf("%d: seen entity id multiple times %d", i, e)
		}
	}

	i := 0

	for e := range seen {
		m.Delete(e)

		if m.Alive(e) {
			t.Fatalf("%d: exp entity (%d) to be dead, but alive", i, e)
		}

		i++

		if i == 1100 {
			break
		}
	}

	for i := 0; i < 20; i++ {
		e := m.Create()
		seen[e]++

		if seen[e] != 1 {
			t.Fatalf("%d: exp entity (%d) to be seen 1 times, but only seen %d", i, e, seen[e])
		}
	}
}

func TestBitPack(t *testing.T) {
	if !testing.Verbose() {
		return
	}

	fmt.Printf("binary count:\n\n")
	val := uint32(0)
	fmt.Printf("%032b\n", val)
	val = 1
	fmt.Printf("%032b\n", val)
	val = 2
	fmt.Printf("%032b\n", val)
	val = 3
	fmt.Printf("%032b\n", val)
	fmt.Printf("\nmax masks:\n\n")

	MaxGenBits := uint32(8)
	GenMask := uint32(-1 ^ (-1 << 8))
	IndexMask := uint32(-1 ^ (-1 << 24))

	fmt.Printf("%032b: %d\n", IndexMask, IndexMask)
	fmt.Printf("%032b: %d\n", GenMask, GenMask)

	fmt.Printf("\nadding:\n\n")

	val = IndexMask
	fmt.Printf("%032b: %d\n", val, val)

	val = IndexMask << MaxGenBits
	fmt.Printf("%032b: %d\n", val, val)

	val = (IndexMask << MaxGenBits) | GenMask
	fmt.Printf("%032b: %d\n", val, val)

	fmt.Printf("\ngetting:\n\n")

	val = (1 << MaxGenBits) | 1
	idx := (val >> MaxGenBits) & IndexMask
	gen := val & GenMask

	fmt.Printf("%032b: %d\n", val, val)
	fmt.Printf("%032b: %d\n", idx, idx)
	fmt.Printf("%032b: %d\n", gen, gen)

	fmt.Println()
}

func TestSliceCopy(t *testing.T) {
	if !testing.Verbose() {
		return
	}
	slice := []int{1, 2, 3, 4, 5}
	//idx := slice[0]
	N := len(slice)
	lastIdx := N - 1

	copy(slice, slice[1:N])
	slice[lastIdx] = 0
	fmt.Println(slice, len(slice), cap(slice))
	slice = slice[:lastIdx]
	fmt.Println(slice, len(slice), cap(slice))
}
