package entity

import (
	"sync/atomic"

	"insmo.com/play/types"
)

const (
	minFreeIndices = 1024
)

var entityCounter uint32

// create a new Entity
func NewEntity() types.Entity {
	return types.Entity(atomic.AddUint32(&entityCounter, 1))
}

// Manager is the Entity Manager. It's purpose is to manage the creation and
// deletion of Entities.
type Manager struct {
	// indices is of N size, where N is the largest entity ID added. Each index
	// stores a generation for the index. The generation is updated when an
	// Entity is deleted.
	indices []uint8

	// freeIndices stores a list of indices which can be reused. To avoid reuse
	// the same index slot too often we put recycled indices in a queue and
	// only resuse the indices if it contains at least minFreeIndices.
	// Since we have 256 generations, an ID will never reappear until its index
	// has run 256 laps through the queue. we must create and destroy at least
	// 256 * minFreeIndices entities until an ID can reappear.
	freeIndices []uint32
}

// NewManager return a new Entity Manager.
func NewManager() *Manager {
	return &Manager{
		indices:     make([]uint8, 0, 2<<(16-1)),
		freeIndices: make([]uint32, 0, 2<<(14-1)),
	}
}

// Create returns a new Entity.
func (m *Manager) Create() types.Entity {
	var idx uint32

	if len(m.freeIndices) > minFreeIndices {
		// pop the first element of the slice (index 0) and shift the slice one
		// element to the left. This makes sure not to decrease the capacity of
		// the slice, golang will use memmove.
		idx = m.freeIndices[0]
		N := len(m.freeIndices)
		lastIdx := N - 1
		copy(m.freeIndices, m.freeIndices[1:N])
		m.freeIndices[lastIdx] = 0
		m.freeIndices = m.freeIndices[:lastIdx]
	} else {
		m.indices = append(m.indices, 0)
		idx = uint32(len(m.indices) - 1)
	}

	return types.NewEntity(idx, m.indices[idx])
}

// Delete marks the given Entity as deleted.
func (m *Manager) Delete(e types.Entity) {
	idx := e.Index()
	m.indices[idx]++
	m.freeIndices = append(m.freeIndices, idx)
}

// Alive tests if the given Entity e has been deleted.
func (m *Manager) Alive(e types.Entity) bool {
	return m.indices[e.Index()] == e.Generation()
}
