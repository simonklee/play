// +build !js

package termlog

import "github.com/buger/goterm"

const maxLog = 24

type termLog struct {
	logcnt int
	log    []string
	prev   []string
	output []string
}

func (t *termLog) Add(v string) {
	t.output = append(t.output, v)
}

func (t *termLog) Log(v string) {
	if len(t.log) == maxLog {
		t.log = t.log[:0+copy(t.log[0:], t.log[1:])]
	}

	t.logcnt++
	t.log = append(t.log, v)
}

func (t *termLog) Flush() {
	if len(t.output) == 0 {
		return
	}

	if len(t.output) == len(t.prev) {
		equal := true

		for i := 0; i < len(t.output) && equal; i++ {
			if t.output[i] != t.prev[i] {
				equal = false
				break
			}
		}

		if equal {
			t.output = t.output[:0]
			return
		}
	}

	t.prev = make([]string, len(t.output))
	copy(t.prev, t.output)

	goterm.Clear()
	goterm.MoveCursor(1, 1)

	for i := len(t.output) - 1; i >= 0; i-- {
		goterm.Println(t.output[i])
	}

	goterm.Println("--------------")
	offset := t.logcnt - len(t.log)

	for i := 0; i < len(t.log); i++ {
		goterm.Printf("%d: %s\n", i+offset, t.log[i])
	}

	t.output = t.output[:0]
	goterm.Flush()
}

var def = &termLog{}

func Add(v string) {
	def.Add(v)
}

func Log(v string) {
	def.Log(v)
}

func Flush() {
	def.Flush()
}

func Clear() {
	goterm.Clear()
}

func init() {
	goterm.Clear()
}
