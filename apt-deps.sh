#!/bin/bash

set -xe

apt-get update -qy

apt-get install -y \
    -o APT::Install-Recommends=false \
    -o APT::Install-Suggests=false \
    libglu1-mesa-dev \
    libgles2-mesa-dev \
    libxrandr-dev \
    libxcursor-dev \
    libxinerama-dev \
    libxi-dev \
    libasound2-dev \
    libglew-dev \
    libalut-dev \
    libxxf86vm-dev


