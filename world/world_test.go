package world_test

import (
	"runtime"
	"testing"

	"insmo.com/play/data"
	"insmo.com/play/data/sprite"
	"insmo.com/play/entity"
	"insmo.com/play/types"
	"insmo.com/play/use"
	"insmo.com/play/world"
)

func TestWorldSystem(t *testing.T) {
	w := world.NewWorld()

	for _, test := range []struct {
		name      string
		component *data.SpaceComponent
	}{
		{
			name: "test one",
			component: &data.SpaceComponent{
				Position: use.Vec{
					X: 1.0,
					Y: 1.0,
				},
				Width:  4.0,
				Height: 4.0,
			},
		},
	} {
		t.Run(test.name, func(t *testing.T) {
			e := entity.NewEntity()
			w.Add(e)
			w.Assign(e, test.component)

			for i, ifa := range w.ByType(&data.SpaceComponent{}) {
				sc := ifa.Component.(*data.SpaceComponent)

				if test.component != sc {
					t.Fatalf("%d exp components to have the same addr got %p exp %p", i, sc, test.component)
				}
			}
		})
	}
}

func TestWorldIssue(t *testing.T) {
	w := world.NewWorld()
	e := entity.NewEntity()
	player := data.Player{
		Name: "Player",
		SpaceComponent: data.SpaceComponent{
			Position: use.Vec{
				X: 0,
				Y: 0,
			},
			Width:  32,
			Height: 32,
		},
		HealthComponent: data.HealthComponent{
			Health: 100,
		},
		SpriteName: sprite.BluebirdFrame0,
	}
	w.Add(e)
	//t.Log("assign Player")
	//w.Assign(e, &player)
	t.Log("assign HealthComponent")
	w.Assign(e, &player.HealthComponent)
	t.Log("assign SpaceComponent")
	t.Logf("all components:\n%v", w.Components(e))
	w.Assign(e, &player.SpaceComponent)
	_, ok := w.ComponentByType(e, &data.SpaceComponent{})

	if !ok {
		t.Fatalf("exp components to have component of type %v", data.GetComponentType(&data.SpaceComponent{}))
	}

	t.Logf("all components:\n%v", w.Components(e))
	w.Assign(e, player.SpriteName)
	t.Log("assign SpriteName")

	_, ok = w.ComponentByType(e, &data.SpaceComponent{})

	if !ok {
		t.Fatalf("exp entity to have component %v, all components:\n%v", data.GetComponentType(&data.SpaceComponent{}), w.Components(e))
	}
}

func BenchmarkAssignComponent(b *testing.B) {
	b.StopTimer()
	w := world.NewWorld()
	CN := 100000
	components := make([]*data.SpaceComponent, 0, 100000)
	for i := 0; i < CN; i++ {
		components = append(components, &data.SpaceComponent{
			Position: use.Vec{
				X: 1.0,
				Y: 1.0,
			},
			Width:  4.0,
			Height: 4.0,
		})
	}
	runtime.GC()
	b.ReportAllocs()
	b.StartTimer()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		e := entity.NewEntity()
		w.Add(e)
		w.Assign(e, components[i%CN])
	}
}

func BenchmarkByTypeComponent(b *testing.B) {
	b.StopTimer()
	w := world.NewWorld()
	CN := 100000
	for i := 0; i < CN; i++ {
		component := &data.SpaceComponent{
			Position: use.Vec{
				X: 1.0,
				Y: 1.0,
			},
			Width:  4.0,
			Height: 4.0,
		}
		e := entity.NewEntity()
		w.Add(e)
		w.Assign(e, component)
	}

	var X float32
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		comps := w.ByType(&data.SpaceComponent{})

		if len(comps) != CN {
			b.Fatal("unexpected len")
		}

		for _, c := range comps {
			X = c.Component.(*data.SpaceComponent).Position.X
		}
	}
	b.StopTimer()

	if X != 1.0 {
		b.Fatal("X not 1.0")
	}
}

func BenchmarkTypeAssert(b *testing.B) {
	b.StopTimer()
	w := world.NewWorld()
	CN := 100000
	for i := 0; i < CN; i++ {
		component := &data.SpaceComponent{
			Position: use.Vec{
				X: 1.0,
				Y: 1.0,
			},
			Width:  4.0,
			Height: 4.0,
		}
		e := entity.NewEntity()
		w.Add(e)
		w.Assign(e, component)
	}

	var X float32
	comps := w.ByType(&data.SpaceComponent{})
	c := comps[32]

	b.StartTimer()
	for i := 0; i < b.N; i++ {
		X = c.Component.(*data.SpaceComponent).Position.X
	}
	b.StopTimer()

	if X != 1.0 {
		b.Fatal("X not 1.0")
	}
}

func BenchmarkLoopSlice(b *testing.B) {
	b.StopTimer()
	CN := 100000
	entities := make([]types.Entity, 0, CN)

	for i := 0; i < CN; i++ {
		entities = append(entities, entity.NewEntity())
	}
	b.StartTimer()

	var ent types.Entity

	for i := 0; i < b.N; i++ {
		for _, e := range entities {
			ent = e
		}
	}
	b.StartTimer()
	if ent <= 0 {
		b.Fatal("e should not be zero")
	}
}

func BenchmarkLoopMap(b *testing.B) {
	b.StopTimer()
	CN := 100000
	entities := make(map[types.Entity]bool, CN)

	for i := 0; i < CN; i++ {
		entities[entity.NewEntity()] = true
	}
	b.StartTimer()

	var ent types.Entity

	for i := 0; i < b.N; i++ {
		for e, _ := range entities {
			ent = e
		}
	}
	b.StartTimer()
	if ent <= 0 {
		b.Fatal("e should not be zero")
	}
}
