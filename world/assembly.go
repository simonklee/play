package world

import (
	"insmo.com/play/data"
	"insmo.com/play/data/sprite"
	"insmo.com/play/entity"
	"insmo.com/play/use"
)

type assembly struct {
	w *World
}

func Assemble(w *World) {
	a := &assembly{w}
	a.Blocks()
	a.Player()
}

func (a *assembly) Blocks() {
	w := a.w
	for i := float32(0); i < 64; i++ {
		for j := float32(0); j < 64; j++ {
			e := entity.NewEntity()
			block := data.Block{
				SpaceComponent: data.SpaceComponent{
					Position: use.Vec{
						X: j * 32.0,
						Y: i * 32.0,
					},
				},
			}
			w.Add(e)
			w.Assign(e, &block)
			w.Assign(e, &block.SpaceComponent)
			sn := sprite.BrickFrame0
			w.Assign(e, &sn)
		}
	}
}

func (a *assembly) Player() {
	w := a.w
	e := entity.NewEntity()
	player := data.Player{
		Name: "Player",
		SpaceComponent: data.SpaceComponent{
			Position: use.Vec{
				X: 0,
				Y: 0,
			},
			Width:  32,
			Height: 32,
		},
		HealthComponent: data.HealthComponent{
			Health: 100,
		},
		SpriteName:    sprite.BluebirdFrame0,
		AnimationName: sprite.BlubirdFlying,
	}
	w.Add(e)
	w.Assign(e, &player)
	w.Assign(e, &player.HealthComponent)
	w.Assign(e, &player.SpaceComponent)
	w.Assign(e, &player.SpriteName)
	w.Assign(e, &player.AnimationName)
}
