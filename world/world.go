package world

import (
	"insmo.com/play/component"
	"insmo.com/play/data"
	"insmo.com/play/types"
)

type World struct {
	// TODO; make entities a map of Entity:[]reflect.Type:
	//
	// entities map[Entity][]reflect.Type
	//
	// Or use a custom reflect.Type lookup table runtime, something like
	//
	// relect.Type:uint16 and uint16:reflect.Type
	//
	// Then we can have map[Entity][]Type (uint16) or we can drop the slice
	// and replace with an encoded value/bitarray. The last optimizations are
	// simply to save memory.
	//
	// To get a component of type t from a single entity e you do
	// types[t][e]
	// entities map[Entity][]reflect.Type
	//
	// entities holds a sorted list of Entity values.
	entities []types.Entity

	// components holds a sorted list of Component's for each Entity value.
	// This makes it fast and easy to find all the components of a given Entity.
	components map[types.Entity][]data.Component

	// TODO; make types a map of maps like so:
	// types    map[reflect.Type]map[Entity]Component
	//
	// types holds a sorted list of Enity+Component for any ComponentType. This
	// makes it easy to find all Component's of a given type.
	types map[types.ComponentType][]data.EntityComponent

	slots *component.SlotsManager
	cm    [data.ComponentTypeCount]*component.Manager
}

func NewWorld() *World {
	w := &World{
		slots:      component.NewSlotsManager(),
		entities:   make([]types.Entity, 0, 2<<15),
		components: make(map[types.Entity][]data.Component, 2<<15),
		types:      make(map[types.ComponentType][]data.EntityComponent, 2<<7),
	}

	for _, t := range data.ComponentTypes {
		w.cm[t] = component.NewManager(w.slots, data.NewComponentsByType(t))
	}

	return w
}

func (w *World) ByType(i interface{}) []data.EntityComponent {
	t := data.GetComponentType(i)
	res, _ := w.types[t]
	return res
}

func imax(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func (w *World) ByTypeIntersect(a, b interface{}) []data.EntityComponentUnion {
	acomps, _ := w.types[data.GetComponentType(a)]
	bcomps, _ := w.types[data.GetComponentType(b)]

	var longest, shortest []data.EntityComponent
	var alongest bool

	if len(acomps) > len(bcomps) {
		longest = acomps
		shortest = bcomps
		alongest = true
	} else {
		longest = bcomps
		shortest = acomps
		alongest = false
	}

	M := len(longest)
	inter := make([]data.EntityComponentUnion, 0, M)
	lookup := make(map[types.Entity]data.EntityComponent, len(shortest))

	for _, ec := range shortest {
		lookup[ec.Entity] = ec
	}

	for i := 0; i < M; i++ {
		lc := longest[i]

		if sc, ok := lookup[lc.Entity]; ok {
			if alongest {
				inter = append(inter, data.EntityComponentUnion{
					A: lc,
					B: sc,
				})
			} else {
				inter = append(inter, data.EntityComponentUnion{
					A: sc,
					B: lc,
				})
			}
		}
	}

	//make(map[types.Entity])
	return inter
}

func (w *World) ByTypeOneMust(i interface{}) data.EntityComponent {
	return w.ByType(i)[0]
}

func (w *World) Components(e types.Entity) []data.Component {
	res, _ := w.components[e]
	return res
}

func (w *World) ComponentByType(e types.Entity, i interface{}) (data.Component, bool) {
	components, _ := w.components[e]

	for _, c := range components {
		if c.Type == data.GetComponentType(i) {
			return c, true
		}
	}

	return data.Component{}, false
}

func (w *World) ComponentByComponentType(e types.Entity, t types.ComponentType) (data.Component, bool) {
	components, _ := w.components[e]

	for _, c := range components {
		if c.Type == t {
			return c, true
		}
	}

	return data.Component{}, false
}

func (w *World) Add(e types.Entity) {
	entities := w.entities
	N := len(entities)

	// fast route
	if N > 0 && entities[N-1] < e {
		entities = append(entities, e)
		return
	}

	idx := bsearch(N, entities, e)
	found := idx >= 0 && idx < N && entities[idx] == e

	// ignore duplicate values
	if found {
		return
	}

	// append the value if idx is greater than types len
	if idx > N {
		entities = append(entities, e)
		return
	} else {
		entities = append(entities, 0)
		copy(entities[idx+1:], entities[idx:])
		entities[idx] = e
	}
}

func (w *World) Remove(e types.Entity) {
	entities := w.entities
	N := len(entities)
	idx := bsearch(N, entities, e)
	found := idx >= 0 && idx < N && entities[idx] == e

	// nothing to remove if it doesn't exist
	if !found {
		return
	}

	entities = append(entities[:idx], entities[idx+1:]...)
	// TODO: remove components for entity
}

func (w *World) Assign(e types.Entity, c interface{}) {
	w.assign(e, c)
}

//func (w *World) Unassign(e Entity, c Component) {
//	w.unassign(e, c)
//}

func (w *World) assign(e types.Entity, c interface{}) {
	t := data.GetComponentType(c)
	ec := data.EntityComponent{e, t, c}
	ct := data.Component{t, c}
	w.types[t] = assignTypes(w.types[t], ec)
	w.components[e] = assignComponent(w.components[e], ct)
}

func assignTypes(types []data.EntityComponent, ec data.EntityComponent) []data.EntityComponent {
	N := len(types)

	// fast route
	if (N > 0 && types[N-1].Entity < ec.Entity) || N == 0 {
		return append(types, ec)
	}

	idx := bsearchEC(N, types, ec)
	found := idx >= 0 && idx < N && types[idx].Entity == ec.Entity

	// ignore duplicate values
	if found {
		return types
	}

	// append the value if idx is greater than types len
	if idx > N {
		types = append(types, ec)
	} else {
		types = append(types, data.EntityComponent{})
		copy(types[idx+1:], types[idx:])
		types[idx] = ec
	}

	return types
}

func assignComponent(components []data.Component, ct data.Component) []data.Component {
	N := len(components)

	// fast route
	if (N > 0 && components[N-1].Type < ct.Type) || N == 0 {
		return append(components, ct)
	}

	idx := bsearchC(N, components, ct.Type)
	found := idx >= 0 && idx < N && components[idx].Type == ct.Type

	// ignore duplicate values
	if found {
		return components
	}

	// append the value if idx is greater than components len
	if idx > N {
		return append(components, ct)
	} else {
		components = append(components, data.Component{})
		copy(components[idx+1:], components[idx:])
		components[idx] = ct
	}

	return components
}

//func (w *World) unassign(e Entity, f ComponentType) {
//	w.entities[e] = w.entities[e].Clear(f)
//	types, ok := w.types[f]
//
//	if !ok {
//		return
//	}
//
//	N := len(types)
//	idx := bsearch(N, types, e)
//	found := idx >= 0 && idx < N && types[idx] == e
//
//	// nothing to remove if it doesn't exist
//	if !found {
//		return
//	}
//
//	w.types[f] = append(types[:idx], types[idx+1:]...)
//}

func bsearch(n int, a []types.Entity, x types.Entity) int {
	// Define f(-1) == false and f(n) == true.
	// Invariant: f(i-1) == false, f(j) == true.
	i, j := 0, n
	for i < j {
		h := i + (j-i)/2 // avoid overflow when computing h
		// i ≤ h < j
		if a[h] < x {
			i = h + 1 // preserves f(i-1) == false
		} else {
			j = h // preserves f(j) == true
		}
	}
	// i == j, f(i-1) == false, and f(j) (= f(i)) == true  =>  answer is i.
	return i
}

func bsearchEC(n int, a []data.EntityComponent, x data.EntityComponent) int {
	// Define f(-1) == false and f(n) == true.
	// Invariant: f(i-1) == false, f(j) == true.
	i, j := 0, n
	for i < j {
		h := i + (j-i)/2 // avoid overflow when computing h
		// i ≤ h < j
		if a[h].Entity < x.Entity {
			i = h + 1 // preserves f(i-1) == false
		} else {
			j = h // preserves f(j) == true
		}
	}
	// i == j, f(i-1) == false, and f(j) (= f(i)) == true  =>  answer is i.
	return i
}

func bsearchC(n int, a []data.Component, x types.ComponentType) int {
	// Define f(-1) == false and f(n) == true.
	// Invariant: f(i-1) == false, f(j) == true.
	i, j := 0, n
	for i < j {
		h := i + (j-i)/2 // avoid overflow when computing h
		// i ≤ h < j
		if a[h].Type < x {
			i = h + 1 // preserves f(i-1) == false
		} else {
			j = h // preserves f(j) == true
		}
	}
	// i == j, f(i-1) == false, and f(j) (= f(i)) == true  =>  answer is i.
	return i
}
