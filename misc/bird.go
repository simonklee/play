package play

//
//import (
//	"math"
//	"math/rand"
//
//	"engo.io/ecs"
//	"engo.io/engo"
//	"engo.io/engo/common"
//)
//
//type Bird struct {
//	ecs.BasicEntity
//	common.RenderComponent
//	common.SpaceComponent
//	common.AnimationComponent
//}
//
//func SpawnBird(w *ecs.World, pos engo.Point, velocity float32) {
//	birdTexture := common.NewSpritesheetFromFile("bluebird.png", 32, 32)
//
//	bird := Bird{
//		BasicEntity: ecs.NewBasic(),
//		SpaceComponent: common.SpaceComponent{
//			Position: pos,
//			Width:    32,
//			Height:   32,
//		},
//		RenderComponent: common.RenderComponent{
//			Drawable: birdTexture.Cell(0),
//			Scale:    engo.Point{1, 1},
//		},
//		AnimationComponent: common.NewAnimationComponent(birdTexture.Drawables(), 0.1),
//	}
//	fly := &common.Animation{Name: "fly", Frames: []int{0, 1, 2, 3}, Loop: true}
//	bird.AnimationComponent.AddDefaultAnimation(fly)
//
//	for _, iface := range w.Systems() {
//		switch system := iface.(type) {
//		case *common.RenderSystem:
//			system.Add(&bird.BasicEntity, &bird.RenderComponent, &bird.SpaceComponent)
//		case *common.AnimationSystem:
//			system.Add(&bird.BasicEntity, &bird.AnimationComponent, &bird.RenderComponent)
//		case *FlySystem:
//			system.Add(&bird.BasicEntity, &bird.SpaceComponent, velocity)
//		}
//	}
//}
//
//type flyComponent struct {
//	*ecs.BasicEntity
//	*common.SpaceComponent
//	velocity float32
//}
//
//type FlySystem struct {
//	entities map[uint64]*flyComponent
//	w        *ecs.World
//}
//
//func NewFlySystem(w *ecs.World) *FlySystem {
//	return &FlySystem{
//		entities: make(map[uint64]*flyComponent),
//		w:        w,
//	}
//}
//
//func (f *FlySystem) Add(e *ecs.BasicEntity, space *common.SpaceComponent, velocity float32) {
//	f.entities[e.ID()] = &flyComponent{e, space, velocity}
//}
//
//func (f *FlySystem) Remove(e ecs.BasicEntity) {
//	delete(f.entities, e.ID())
//}
//
//func (f *FlySystem) Update(dt float32) {
//	w := engo.GameWidth()
//
//	for _, e := range f.entities {
//		if e.SpaceComponent.Position.X > w {
//			e.SpaceComponent.Position.X = -32
//		}
//
//		e.SpaceComponent.Position.X += e.velocity * dt * (rand.Float32() + 0.5)
//		Y := e.SpaceComponent.Position.Y
//		Y = Y + float32(math.Sin(float64(e.SpaceComponent.Position.X)*0.01))*3
//		e.SpaceComponent.Position.Y = Y
//	}
//}
