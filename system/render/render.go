package render

import (
	"fmt"
	"image/color"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"

	"insmo.com/play/resource"
	"insmo.com/play/use"
	"insmo.com/play/window"
	"insmo.com/play/world"
)

type RenderSystem struct {
	res          *resource.Resources
	world        *world.World
	camera       *CameraSystem
	spriteRender *SpriteRender
}

func New(world *world.World, res *resource.Resources, state *window.State) *RenderSystem {
	// TODO: map bounds defined by scene size
	mapBounds := use.R(0, 0, 2048, 2048)
	camera := NewCameraSystem(world, state, mapBounds)

	return &RenderSystem{
		res:    res,
		world:  world,
		camera: camera,
		spriteRender: &SpriteRender{
			res: res, world: world, camera: camera,
		},
	}
}

func (s *RenderSystem) Update(dt float32) error {
	return s.camera.Update(dt)
}

func (s *RenderSystem) Render(screen *ebiten.Image, dt float32) error {
	screen.Fill(color.RGBA{0x80, 0xa0, 0xc0, 0xff})
	s.spriteRender.Render(screen, dt)
	ebitenutil.DebugPrint(screen, fmt.Sprintf("%.2f", ebiten.CurrentFPS()))
	return nil
}
