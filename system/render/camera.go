// TODO: make camera update based on state object
package render

import (
	"fmt"

	"github.com/go-gl/mathgl/mgl32"
	"insmo.com/play/data"
	"insmo.com/play/entity"
	"insmo.com/play/types"
	"insmo.com/play/use"
	"insmo.com/play/window"
	"insmo.com/play/world"
)

type CameraSystem struct {
	world  *world.World
	camera *data.Camera
	state  *window.State
}

func NewCameraSystem(world *world.World, state *window.State, bounds use.Rect) *CameraSystem {
	scale := state.Scale()
	size := state.Size()
	offsetX, offsetY := size.W()/2, size.H()/2
	cameraBounds := use.R(
		bounds.Min.X+(offsetX/scale),
		bounds.Min.Y+(offsetY/scale),
		bounds.Max.X-(offsetX/scale),
		bounds.Max.Y-(offsetY/scale),
	)

	//termlog.Log(fmt.Sprintf("camera offset: %v:%v", offsetX, offsetY))
	//termlog.Log(fmt.Sprintf("world size: %s", bounds))
	//termlog.Log(fmt.Sprintf("window size: %s", state.Size()))
	//termlog.Log(fmt.Sprintf("camera bounds: %s", cameraBounds))
	//termlog.Log(fmt.Sprintf("camera start: %s", cameraBounds.Center()))

	player := world.ByTypeOneMust(&data.Player{})
	e := entity.NewEntity()
	camera := data.Camera{
		Scale:       scale,
		WorldBounds: cameraBounds,
		WorldPos:    cameraBounds.Center(),
		Width:       size.W(),
		Height:      size.H(),
		Tracking:    player.Entity,
	}
	world.Add(e)
	world.Assign(e, &camera)

	return &CameraSystem{
		world:  world,
		camera: &camera,
		state:  state,
	}
}

func (s *CameraSystem) TrackEntity(e types.Entity) {
	s.camera.Tracking = e
}

func (s *CameraSystem) StopTrackEntity() {
	s.camera.Tracking = 0
}

func (s *CameraSystem) Update(dt float32) error {
	c := s.camera
	if c.Tracking != 0 {
		comp, ok := s.world.ComponentByType(c.Tracking, &data.SpaceComponent{})

		if !ok {
			return fmt.Errorf("camera: cannot track entity without SpaceComponent %d", c.Tracking)
		}

		sc := comp.Component.(*data.SpaceComponent)
		cx := sc.Position.X //+ sc.Width/2
		cy := sc.Position.Y //+ sc.Height/2

		x := cx - (s.state.Size().W() / 2)
		y := cy - (s.state.Size().H() / 2)

		s.moveToX(x)
		s.moveToY(y)

		//termlog.Add(fmt.Sprintf("camera pos: %v:%v => %v:%v => %v:%v", sc.Position.X, sc.Position.Y, x, y, c.WorldPos.X, c.WorldPos.Y))
	}

	return nil
}

func (s *CameraSystem) moveX(value float32) {
	c := s.camera

	if c.WorldPos.X+(value*c.Scale) > c.WorldBounds.Max.X*c.Scale {
		c.WorldPos.X = c.WorldBounds.Max.X * c.Scale
	} else if c.WorldPos.X+(value*c.Scale) < c.WorldBounds.Min.X*c.Scale {
		c.WorldPos.X = c.WorldBounds.Min.X * c.Scale
	} else {
		c.WorldPos.X += value * c.Scale
	}
}

func (s *CameraSystem) moveY(value float32) {
	c := s.camera

	if c.WorldPos.Y+(value*c.Scale) > c.WorldBounds.Max.Y*c.Scale {
		c.WorldPos.Y = c.WorldBounds.Max.Y * c.Scale
	} else if c.WorldPos.Y+(value*c.Scale) < c.WorldBounds.Min.Y*c.Scale {
		c.WorldPos.Y = c.WorldBounds.Min.Y * c.Scale
	} else {
		c.WorldPos.Y += value * c.Scale
	}
}

func (s *CameraSystem) moveToX(location float32) {
	c := s.camera
	c.WorldPos.X = mgl32.Clamp(location*c.Scale, c.WorldBounds.Min.X*c.Scale, c.WorldBounds.Max.X*c.Scale)
}

func (s *CameraSystem) moveToY(location float32) {
	c := s.camera
	c.WorldPos.Y = mgl32.Clamp(location*c.Scale, c.WorldBounds.Min.Y*c.Scale, c.WorldBounds.Max.Y*c.Scale)
}

func (cam *CameraSystem) centerCam(x, y float32) {
	cam.moveToX(x)
	cam.moveToY(y)
}
