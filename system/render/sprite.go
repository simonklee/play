package render

import (
	"image"

	"github.com/hajimehoshi/ebiten"
	"insmo.com/play/data"
	"insmo.com/play/data/sprite"
	"insmo.com/play/resource"
	"insmo.com/play/world"
)

type SpriteRender struct {
	res    *resource.Resources
	world  *world.World
	camera *CameraSystem
}

func (s *SpriteRender) Render(screen *ebiten.Image, dt float32) error {
	camera := s.camera.camera
	op := &ebiten.DrawImageOptions{}
	view := &image.Rectangle{}
	op.SourceRect = view

	for _, comp := range s.world.ByTypeIntersect(&data.SpaceComponent{}, sprite.SpriteName(0)) {
		op.GeoM.Reset()

		wpos := comp.A.Component.(*data.SpaceComponent)
		spriteName := comp.B.Component.(*sprite.SpriteName)

		pos := camera.WorldToScreen(wpos.Position)
		op.GeoM.Translate(pos.F64())

		sprite := sprite.GetSprite(*spriteName)
		source := s.res.GetImage(sprite.Resource)

		view.Min.X = int(sprite.View.Min.X)
		view.Min.Y = int(sprite.View.Min.Y)
		view.Max.X = int(sprite.View.Max.X)
		view.Max.Y = int(sprite.View.Max.Y)

		screen.DrawImage(source, op)
	}

	return nil
}
