package system

import (
	"fmt"

	"insmo.com/play/types"
)

type damage struct {
	e      types.Entity
	amount int
}

type DamageSystem struct {
	queue []damage
}

var _ System = &DamageSystem{}

func NewDamageSystem() *DamageSystem {
	return &DamageSystem{make([]damage, 0)}
}

func (d *DamageSystem) InflictDamage(e types.Entity, amount int) {
	d.queue = append(d.queue, damage{e, amount})
}

func (d *DamageSystem) Update(dt float32) error {
	for _, msg := range d.queue {
		fmt.Println(msg)
	}

	return nil
}
