package system

import (
	"fmt"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/inpututil"
	"insmo.com/play/data"
	"insmo.com/play/termlog"
	"insmo.com/play/use"
	"insmo.com/play/world"
)

type ControlSystem struct {
	world *world.World
}

func (s ControlSystem) Update(dt float32) error {
	ipl := s.world.ByTypeOneMust(&data.Player{})
	player := ipl.Component.(*data.Player)

	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonLeft) {
		icam := s.world.ByTypeOneMust(&data.Camera{})
		camera := icam.Component.(*data.Camera)

		x, y := ebiten.CursorPosition()
		screenPos := use.V(float32(x), float32(y))
		worldPos := camera.ScreenToWorld(screenPos)

		// TODO: set target intention
		player.Position.X = float32(worldPos.X) - (player.Width / 2)
		player.Position.Y = float32(worldPos.Y) - (player.Height / 2)

		termlog.Log(fmt.Sprintf("camera: %v:%v, screen click: %s world click: %s", camera.WorldPos.X, camera.WorldPos.Y, screenPos, worldPos))
	}

	termlog.Add(fmt.Sprintf("player pos: %v:%v", player.Position.X, player.Position.Y))
	return nil
}

var _ System = ControlSystem{}
