package animation

import (
	"insmo.com/play/data/sprite"
	"insmo.com/play/types"
	"insmo.com/play/world"
)

type AnimationSystem struct {
	world  *world.World
	active map[types.Entity]*Timeline
}

func NewSystem(w *world.World) *AnimationSystem {
	return &AnimationSystem{
		world:  w,
		active: make(map[types.Entity]*Timeline),
	}
}

func (s *AnimationSystem) Update(dt float32) error {
	for _, comp := range s.world.ByTypeIntersect(sprite.AnimationName(0), sprite.SpriteName(0)) {
		animationName := comp.A.Component.(*sprite.AnimationName)
		spriteName := comp.B.Component.(*sprite.SpriteName)

		if _, ok := s.active[comp.A.Entity]; !ok {
			anim := sprite.GetAnimation(*animationName)
			s.active[comp.A.Entity] = Play(anim).Loop()
		}

		t := s.active[comp.A.Entity]
		t.Update(dt)
		newSprite := t.SpriteName()
		*spriteName = newSprite
	}

	return nil
}

type Timeline struct {
	pos  float32
	rate float32
	id   sprite.AnimationName
	loop bool
	rev  bool
}

func (t *Timeline) Update(dt float32) {
	t.pos += dt * t.rate
	anim := sprite.GetAnimation(t.id)

	// loop
Loop:
	if t.loop && t.pos >= anim.Duration {
		if anim.Duration < 0.01 {
			t.pos = 0
		} else {
			t.pos -= anim.Duration
			goto Loop
		}
	}
}

func (t *Timeline) revframeidx() int {
	anim := sprite.GetAnimation(t.id)
	var durAtFrame float32
	var i int

	for i = len(anim.Frames) - 1; i >= 0; i-- {
		durAtFrame += anim.Frames[i].Duration

		if durAtFrame > t.pos {
			break
		}
	}

	return i
}

func (t *Timeline) frameidx() int {
	if t.rev {
		return t.revframeidx()
	}

	anim := sprite.GetAnimation(t.id)
	var durAtFrame float32
	var idx int

	for i, frame := range anim.Frames {
		durAtFrame += frame.Duration
		idx = i

		if durAtFrame > t.pos {
			break
		}
	}

	return idx
}

func (t *Timeline) Sprite() sprite.Sprite {
	anim := sprite.GetAnimation(t.id)
	return anim.Frames[t.frameidx()].Sprite
}

func (t *Timeline) SpriteName() sprite.SpriteName {
	anim := sprite.GetAnimation(t.id)
	return anim.Frames[t.frameidx()].Sprite.Identifier
}

func (t *Timeline) Reverse() *Timeline {
	t.rev = true
	return t
}

func (t *Timeline) Loop() *Timeline {
	t.loop = true
	return t
}

func (t *Timeline) Rate(rate float32) *Timeline {
	t.rate = rate
	return t
}

func Play(a *sprite.Animation) *Timeline {
	return &Timeline{
		id:   a.Identifier,
		pos:  0,
		rate: 1,
		loop: false,
		rev:  false,
	}
}
