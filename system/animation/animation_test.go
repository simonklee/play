package animation_test

import (
	"log"
	"testing"

	"insmo.com/play/data/sprite"
	"insmo.com/play/system/animation"
)

func TestPlay(t *testing.T) {
	pidgon := sprite.GetAnimation(sprite.PidgonIdling)
	frameCount := len(pidgon.Frames) - 1

	for _, test := range []struct {
		anim   *sprite.Animation
		dt     float32
		expIdx int
		name   string
		rev    bool
		loop   bool
		rate   float32
	}{
		{
			name:   "first frame should be 0",
			anim:   pidgon,
			dt:     0,
			expIdx: 0,
		},
		{
			name:   "exact frame update should move cell to next frame",
			anim:   pidgon,
			dt:     pidgon.Frames[0].Duration,
			expIdx: 1,
		},
		{
			name:   "don't loop",
			anim:   pidgon,
			dt:     pidgon.Duration,
			expIdx: len(pidgon.Frames) - 1,
		},
		{
			name:   "loop to first frame when time has proceeded",
			anim:   pidgon,
			dt:     pidgon.Duration,
			expIdx: 0,
			loop:   true,
		},
		{
			name:   "loop to multiple times when dt is large",
			anim:   pidgon,
			dt:     (pidgon.Duration * 2) + pidgon.Frames[0].Duration,
			expIdx: 1,
			loop:   true,
		},
		{
			name:   "playback in reverse",
			anim:   pidgon,
			dt:     0,
			expIdx: frameCount,
			rev:    true,
		},
		{
			name:   "playback in reverse should move to cell to prev frame",
			anim:   pidgon,
			dt:     pidgon.Frames[frameCount].Duration,
			expIdx: frameCount - 1,
			rev:    true,
		},
		{
			name:   "playback in reverse should loop",
			anim:   pidgon,
			dt:     pidgon.Duration,
			expIdx: frameCount,
			rev:    true,
			loop:   true,
		},
	} {
		t.Run(test.name, func(t *testing.T) {
			anim := test.anim
			playback := animation.Play(anim)

			if test.loop {
				playback.Loop()
			}

			if test.rev {
				playback.Reverse()
			}

			if test.rate != 0.0 {
				playback.Rate(test.rate)
			}

			playback.Update(test.dt)

			got := playback.Sprite()
			exp := anim.Frames[test.expIdx].Sprite

			if got != exp {
				log.Fatalf("exp frame at idx %d %v got %v", test.expIdx, exp, got)
			}
		})
	}
}
