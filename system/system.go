package system

import (
	"time"

	"github.com/hajimehoshi/ebiten"
	"insmo.com/play/resource"
	"insmo.com/play/system/animation"
	"insmo.com/play/system/render"
	"insmo.com/play/window"
	"insmo.com/play/world"
)

const maxMetricCount = 60 * 60

type System interface {
	Update(dt float32) error
}

type RenderSystem interface {
	Render(screen *ebiten.Image, dt float32) error
}

var _ RenderSystem = &render.RenderSystem{}

type RunSystems struct {
	world           *world.World
	state           *window.State
	damageSystem    *DamageSystem
	inputSystem     *InputSystem
	controlSystem   *ControlSystem
	animationSystem *animation.AnimationSystem
	renderSystem    *render.RenderSystem
}

func NewRunSystems(world *world.World, res *resource.Resources, state *window.State) *RunSystems {
	return &RunSystems{
		world:           world,
		damageSystem:    NewDamageSystem(),
		inputSystem:     &InputSystem{},
		controlSystem:   &ControlSystem{world},
		animationSystem: animation.NewSystem(world),
		renderSystem:    render.New(world, res, state),
	}
}

func (r *RunSystems) Run(dt float32, sys System, typ systemType) error {
	start := time.Now()
	err := sys.Update(dt)
	_ = time.Since(start)

	//if duration > time.Microsecond*10 {
	//	log.Printf("running time for system %v was %v\n", typ, duration)
	//}

	return err
}

func (r *RunSystems) Update(dt float32) error {
	if err := r.Run(dt, r.inputSystem, inputSystemType); err != nil {
		return err
	}

	if err := r.Run(dt, r.controlSystem, controlSystemType); err != nil {
		return err
	}

	if err := r.Run(dt, r.damageSystem, damageSystemType); err != nil {
		return err
	}

	if err := r.Run(dt, r.animationSystem, animationSystemType); err != nil {
		return err
	}

	if err := r.Run(dt, r.renderSystem, renderSystemType); err != nil {
		return err
	}

	return nil
}

func (r *RunSystems) Render(screen *ebiten.Image, dt float32) error {
	return r.renderSystem.Render(screen, dt)
}
