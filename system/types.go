package system

//go:generate stringer -type=systemType types.go
type systemType uint32

const (
	damageSystemType systemType = iota + 1
	inputSystemType
	renderSystemType
	cameraSystemType
	controlSystemType
	animationSystemType
)
