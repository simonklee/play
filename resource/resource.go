package resource

import (
	"bytes"
	"image"

	"github.com/hajimehoshi/ebiten"
	"insmo.com/play/assets"
)

type Resource uint8

type Resources struct {
	img map[Resource]*ebiten.Image
}

func New() *Resources {
	return &Resources{make(map[Resource]*ebiten.Image)}
}

func (r *Resources) LoadImages(images ...Resource) error {
	for _, res := range images {
		b, err := assets.Asset(resourceToPath[res])

		if err != nil {
			return err
		}

		origImg, _, err := image.Decode(bytes.NewReader(b))

		if err != nil {
			return err
		}

		img, _ := ebiten.NewImageFromImage(origImg, ebiten.FilterNearest)
		r.img[res] = img
	}

	//fontImgs := map[rune]*ebiten.Image{}
	//for n := 48; n <= 57; n++ {
	//	b, err := assets.Asset(fmt.Sprintf("resources/font/%d.png", n))
	//	if err != nil {
	//		return err
	//	}
	//	origImg, _, err := image.Decode(bytes.NewReader(b))
	//	if err != nil {
	//		return err
	//	}
	//	img, err := ebiten.NewImageFromImage(origImg, ebiten.FilterNearest)
	//	if err != nil {
	//		return err
	//	}
	//	fontImgs[rune(n)] = img
	//}
	//g.font = font.NewFont(fontImgs)
	return nil
}

func (r *Resources) GetImage(res Resource) *ebiten.Image {
	return r.img[res]
}

func (r *Resources) LoadAll() error {
	return r.LoadImages(imageResources...)
}

func Load() (*Resources, error) {
	r := New()
	err := r.LoadAll()
	return r, err
}

func LoadAsync() (chan *Resources, chan error) {
	errch := make(chan error)
	rch := make(chan *Resources)

	go func() {
		r := New()
		err := r.LoadAll()

		if err != nil {
			errch <- err
		} else {
			rch <- r
		}

		close(errch)
		close(rch)
	}()

	return rch, errch
}
