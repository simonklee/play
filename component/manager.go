package component

import (
	"sort"

	"insmo.com/play/data"
	"insmo.com/play/types"
)

// Manager is a manager for slots and Components. It keeps track of
// the position of Components and Slots indices, and the mapping between them.
type Manager struct {
	slots *SlotsManager
	// dense contains Position's packed in a dense array. It's were we store
	// the actual data, while slots are used as lookup table to the dense
	// Position's.
	dense types.Components

	// denseToSlots is a lookup table from dense to slots
	denseToSlots []uint32

	denseSortable types.Sortable
	sorted        bool
	sortable      bool
}

// NewManager return a new ComponentID Manager.
func NewManager(slots *SlotsManager, dense types.Components) *Manager {
	ds, sortable := dense.(types.Sortable)
	return &Manager{
		slots:         slots,
		dense:         dense,
		denseSortable: ds,
		sortable:      sortable,
	}
}

// createComponentID returns a new ComponentID.
func (m *Manager) createComponentID(denseIdx uint32, typ types.ComponentType) types.ComponentID {
	i := m.slots.GetIndex()
	gen := m.slots.Slots[i].Generation()
	m.slots.Slots[i] = newInternalLink(denseIdx, gen)
	return types.NewComponentID(i, gen, uint8(typ))
}

// Add assumes a given component was added to the components dense array.
func (m *Manager) Add(typ types.ComponentType) types.ComponentID {
	m.sorted = false
	denseIdx := m.dense.Len() - 1
	id := m.createComponentID(uint32(denseIdx), typ)

	if denseIdx > len(m.denseToSlots) {
		panic("denseIdx should either be N or < len of denseToSlots")
	}

	if denseIdx == len(m.denseToSlots) {
		m.denseToSlots = append(m.denseToSlots, uint32(id.Index()))
	} else {
		m.denseToSlots[denseIdx] = uint32(id.Index())
	}

	return id
}

// AddValue is like Add, but it adds the value to the components slice
func (m *Manager) AddValue(value interface{}) types.ComponentID {
	m.dense.Add(value)
	return m.Add(data.GetComponentType(value))
}

// Delete marks the given ComponentID as deleted. When a component is deleted
// it's generation is incremented and it's internal position in the dense array
// is moved to the end, decrementing the lenght of the dense array and
// effectivly deleting the item.
func (m *Manager) Delete(c types.ComponentID) {
	m.sorted = false

	// increment the generation of the slotIdx
	idx := c.Index()
	ilink := m.slots.Slots[idx]
	m.slots.PutIndex(idx)

	// move the deleted item to the end.
	N := m.dense.Len()
	lastIdx := N - 1
	delIdx := ilink.Index()
	m.swap(delIdx, uint32(lastIdx))

	// decrement length of dense and denseToSlots.
	m.dense.PopBack()
	m.denseToSlots = m.denseToSlots[:N-1]
}

// swap internally swaps i and j in dense. It makes sure to update the internal
// bookkeeping in addition to swap the data.
func (m *Manager) swap(i, j uint32) {
	// swap the slots internal links of i and j. We need to create internal
	// links which have have their index swapped.
	si, sj := m.denseToSlots[i], m.denseToSlots[j]
	ilink := newInternalLink(j, m.slots.Slots[si].Generation())
	jlink := newInternalLink(i, m.slots.Slots[sj].Generation())
	m.slots.Slots[si], m.slots.Slots[sj] = ilink, jlink

	// swap i and j in the dense components.
	m.dense.Swap(int(i), int(j))

	// swap i and j in the denseToSlots lookup table.
	m.denseToSlots[i], m.denseToSlots[j] = m.denseToSlots[j], m.denseToSlots[i]
}

// Alive tests if the given ComponentID c has been deleted.
func (m *Manager) Alive(c types.ComponentID) bool {
	return m.slots.Alive(c)
}

func (m *Manager) Swap(i, j int) {
	m.swap(uint32(i), uint32(j))
}

func (m *Manager) Less(i, j int) bool {
	if m.sortable {
		return m.denseSortable.Less(i, j)
	}

	return false
}

func (m *Manager) Len() int {
	return m.dense.Len()
}

func (m *Manager) Sort() {
	if m.sorted || !m.sortable {
		return
	}

	m.sorted = true
	sort.Sort(m)
}
