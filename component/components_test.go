package component_test

import (
	"sort"
	"testing"

	"insmo.com/play/types"
)

type Positions []Position

func (d *Positions) Swap(i, j int) {
	(*d)[i], (*d)[j] = (*d)[j], (*d)[i]
}

func (d *Positions) Less(i, j int) bool {
	return (*d)[i].X < (*d)[j].X
}

func (d Positions) Len() int {
	return len(d)
}

func (d *Positions) PopBack() {
	*d = (*d)[:len(*d)-1]
}

func (d *Positions) Add(v interface{}) {
	*d = append((*d), v.(Position))
}

var _ types.Components = &Positions{}

func TestPositions(t *testing.T) {
	slice := []Position{{1, 1}, {2, 2}, {3, 3}}
	comps := Positions{}

	for _, p := range slice {
		comps.Add(p)
	}

	comps.Swap(0, 1)

	if comps[0].X != 2 {
		t.Fatalf("expected items to be swapped")
	}

	sort.Sort(&comps)

	if comps[0].X != 1 {
		t.Fatalf("expected items to be sorted")
	}

	comps.PopBack()

	if comps.Len() != 2 {
		t.Fatalf("expected 2 items, got %d", comps.Len())
	}

	// slice has an address to the first slice header
	slice = comps

	if len(slice) != 2 {
		t.Fatalf("expected 2 items, got %d", len(slice))
	}

	comps = append(comps, Position{4, 4})

	if comps.Len() != 3 {
		t.Fatalf("expected 3 items, got %d", comps.Len())
	}

	comps.Add(Position{5, 5})

	if comps.Len() != 4 || len(comps) != 4 {
		t.Fatalf("expected 4 items, got %d", comps.Len())
	}
}

func BenchmarkPassByValueRange(b *testing.B) {
	b.StopTimer()
	CN := 1000
	entities := make([]Position, 0, CN)

	for i := 0; i < CN; i++ {
		entities = append(entities, Position{})
	}

	positions := Positions(entities)
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		for j := 0; j < positions.Len(); j++ {
			positions[j].X = j + 1
		}
	}

	b.StopTimer()
	if entities[CN-1].X <= 0 {
		b.Fatal("X should not be zero")
	}
}
