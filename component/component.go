package component

import "insmo.com/play/types"

// ComponentID to Component Type map
// world -> GetComponents(c) []ComponentID
// world -> ComponentPositions() []Position

const (
	minFreeSlots = 1024
)

// internalLink is a lookup from slots to dense, but it includes a generation
// count. internalLink should be a type alias for types.ComponentID as their memory
// layout is equal.
type internalLink uint32

// newInternalLink assembles an internalLink by packing the index and generation
func newInternalLink(idx uint32, gen uint8) internalLink {
	return internalLink(types.NewComponentID(idx, gen, 0))
}

// Index returns the index value for given internalLink (internal).
func (c internalLink) Index() uint32 {
	return types.ComponentID(c).Index()
}

// Type returns the type for given internalLink.
func (c internalLink) Type() types.ComponentType {
	return types.ComponentID(c).Type()
}

// Generation returns the generation for given internalLink (internal).
func (c internalLink) Generation() uint8 {
	return types.ComponentID(c).Generation()
}
