package component_test

import (
	"fmt"
	"reflect"
	"testing"
	"unsafe"

	"insmo.com/play/component"
	"insmo.com/play/types"
)

type Position struct {
	X, Y int
}

func TestComponent(t *testing.T) {
	slots := component.NewSlotsManager()
	dense := Positions{}
	m := component.NewManager(slots, &dense)
	seen := make(map[types.ComponentID]int, 4048)
	components := make([]types.ComponentID, 0, 4048)

	for i := 0; i <= 8; i++ {
		p := Position{i, i}
		dense = append(dense, p)
		c := m.Add(types.ComponentType(1))
		seen[c]++
		components = append(components, c)

		if seen[c] > 1 {
			t.Fatalf("%d: seen component id multiple times %d", i, c)
		}
	}

	i := 0

	for _, c := range components {
		m.Delete(c)

		if m.Alive(c) {
			t.Fatalf("%d: exp component (%d) to be dead, but alive", i, c)
		}

		i++

		if i == 1100 {
			break
		}
	}

	for i := 0; i < 20; i++ {
		p := Position{i, i}
		dense = append(dense, p)
		c := m.Add(types.ComponentType(1))
		seen[c]++

		if seen[c] != 1 {
			t.Fatalf("%d: exp component (%d) to be seen 1 times, but only seen %d", i, c, seen[c])
		}
	}
}

func TestBitPack(t *testing.T) {
	if !testing.Verbose() {
		return
	}

	fmt.Printf("binary count:\n\n")
	val := uint32(0)
	fmt.Printf("%032b\n", val)
	val = 1
	fmt.Printf("%032b\n", val)
	val = 2
	fmt.Printf("%032b\n", val)
	val = 3
	fmt.Printf("%032b\n", val)
	fmt.Printf("\nmax masks:\n\n")

	MaxGenBits := uint32(6)
	MaxTypBits := uint32(8)
	GenMask := uint32(-1 ^ (-1 << 6))
	TypMask := uint32(-1 ^ (-1 << 8))
	IndexMask := uint32(-1 ^ (-1 << 18))

	fmt.Printf("%032b: %d\n", GenMask, GenMask)
	fmt.Printf("%032b: %d\n", TypMask, TypMask)
	fmt.Printf("%032b: %d\n", IndexMask, IndexMask)

	fmt.Printf("\nadding:\n\n")

	//return ComponentID((idx << (maxGenBits)) | uint32(gen))
	val = IndexMask
	fmt.Printf("%032b: %d\n", val, val)

	val = IndexMask << (MaxTypBits + MaxGenBits)
	fmt.Printf("%032b: %d\n", val, val)

	val = IndexMask<<(MaxTypBits+MaxGenBits) | (TypMask << MaxGenBits)
	fmt.Printf("%032b: %d\n", val, val)

	val = IndexMask<<(MaxTypBits+MaxGenBits) | (TypMask << MaxGenBits) | GenMask
	fmt.Printf("%032b: %d\n", val, val)

	fmt.Printf("\ngetting:\n\n")

	index := uint32(1968)
	typ := uint32(78)
	gen := uint32(30)

	packed := (index << (MaxTypBits + MaxGenBits)) | (typ << MaxGenBits) | gen
	gotindex := (packed >> (MaxTypBits + MaxGenBits)) & IndexMask
	gottyp := (packed >> (MaxGenBits)) & TypMask
	gotgen := packed & GenMask

	fmt.Printf("%032b: %d\n", packed, packed)
	fmt.Printf("%032b: %d\n", gotindex, gotindex)
	fmt.Printf("%032b: %d\n", gottyp, gottyp)
	fmt.Printf("%032b: %d\n", gotgen, gotgen)

	if index != gotindex || typ != gottyp || gen != gotgen {
		t.Fatalf("bitpacked exp values failed")
	}

	fmt.Println()
}

func TestSliceCopy(t *testing.T) {
	if !testing.Verbose() {
		return
	}
	slice := []uint32{1, 2, 3, 4, 5}
	//idx := slice[0]
	N := len(slice)
	lastIdx := N - 1

	copy(slice, slice[1:N])
	slice[lastIdx] = 0
	fmt.Println(slice, len(slice), cap(slice))
	slice = slice[:lastIdx]
	fmt.Println(slice, len(slice), cap(slice))
}

func TestSliceInplaceConversion(t *testing.T) {
	//slice := []Position{{1, 1}, {2, 2}, {3, 3}}
	slice := []uint32{1, 2, 3}

	header := *(*reflect.SliceHeader)(unsafe.Pointer(&slice))

	data := *(*[][unsafe.Sizeof(uint32(0))]byte)(unsafe.Pointer(&header))

	data[0], data[1] = data[1], data[0]

	realdata := *(*[]uint32)(unsafe.Pointer(&data))

	fmt.Println(header)
	fmt.Println(data, len(data), cap(data))
	fmt.Println(realdata, len(realdata), cap(realdata))
}
