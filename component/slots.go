package component

import "insmo.com/play/types"

// SlotsManager is used at the world level to manage single array of all
// componentID's. Each ComponentManager has a reference to SlotsManager and
// will use that to aquire free slots. You only need a reference to a
// ComponentManager to add and remove Components to the world.
type SlotsManager struct {
	// Slots is of N size, where N is the largest componentID added. Each index
	// stores a generation for the index. The generation is updated when an
	// ComponentID is deleted, to guard against use after free.
	Slots []internalLink

	// freeSlots stores a list of slots which can be reused. To avoid reuse the
	// same index slot too often we put recycled slots in a queue and only
	// reuse the slots if it contains at least minFreeSlots.  Since we have
	// 256 generations, an ID will never reappear until its index has run 256
	// laps through the queue. we must create and destroy at least 256 *
	// minFreeSlots ComponentID's until an ID can reappear.
	freeSlots []uint32
}

func NewSlotsManager() *SlotsManager {
	return &SlotsManager{
		Slots:     make([]internalLink, 0, 2<<(16-1)),
		freeSlots: make([]uint32, 0, 2<<(14-1)),
	}
}

func (m *SlotsManager) GetIndex() (index uint32) {
	if len(m.freeSlots) > minFreeSlots {
		// pop the first element of the slice (index 0) and shift the slice one
		// element to the left. This makes sure not to decrease the capacity of
		// the slice, golang will use memmove.
		index = m.freeSlots[0]
		N := len(m.freeSlots)
		lastIdx := N - 1
		copy(m.freeSlots, m.freeSlots[1:N])
		m.freeSlots[lastIdx] = 0
		m.freeSlots = m.freeSlots[:lastIdx]
	} else {
		m.Slots = append(m.Slots, 0)
		index = uint32(len(m.Slots) - 1)
	}

	return index
}

// PutIndex free's the slot by incrementing the generation of the link at
// slots[i] and adding it back to the freeSlots queue.
func (m *SlotsManager) PutIndex(i uint32) {
	ilink := m.Slots[i]
	m.Slots[i] = newInternalLink(i, ilink.Generation()+1)
	m.freeSlots = append(m.freeSlots, i)
}

// Alive tests if the given ComponentID c has been deleted.
func (m *SlotsManager) Alive(c types.ComponentID) bool {
	return m.Slots[c.Index()].Generation() == c.Generation()
}
