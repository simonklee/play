PLAY
====

A friendly game about birds, seeds and and nature.


Preface
-------

You are set into the world with seeds and a shovel. Plant your seeds, water
them, take care of them.


Mechanics
---------


**Time** is broken into units of days, months, seasons and years. Each unity of
time has its cycles.

_Day_:

    Raise organisms for food or raw materials.

_Month_:

    Harvest and collect seeds.
    Plant seeds for next month.

_Season_:

    Change your plants for the coming season.

_Year_:

    Measure your progress.


**Systems** are units of logic which determine how items and events behave in
the world.

Examples

_Water System_: Defines how components who have water in them or how components
touch water behave. Water components have temperature.

_Rain System_: Defines how and when drops of water (rain) is emitted to the
world. Rain system emit water.

_Soil System_: Defines how soil behaves. Soil system listens for water. If water
is added to a soil component it's water lever increases. A soil component
consists of soil type, water, temperature, hardness.

_Day/Night Cycle System_: Listens for changes in time to determine if it's
morning, midday, afternoon, evening, night. Day/Night has a sun component which
affects the global light in the world. It emits changes in temperature (hot day,
cold night)

_Temperature System_: Defines how temperature changes in the world. It listens
for changes in temperature from inputs like time of year, day/night cycle and
emits a global temperature which affects components.

_Growth System_: 


Data Structure
--------------


`Entity` is a unique object. Each object in the world is tied to an `Entity`. In
practice an `Entity` is simply a uint32. An `Enitity` may have _zero or more_
`Component`.

`Component` is a struct of properties.

`System` acts on `Component` types.

`World` has a collection of `Entity`, `Component` and `System`.
build


Docker update
-------------

```shell
docker build -t insmo/play .
```

login:

```shell
docker login
```

tag

```shell
docker tag insmo/play insmo/play
```

push

```shell
docker push insmo/play
```
