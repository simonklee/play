package window

import (
	"sync"

	"github.com/hajimehoshi/ebiten"
	"insmo.com/play/use"
)

type State struct {
	title      string
	width      float32
	height     float32
	scale      float32
	fullscreen bool
	mux        sync.RWMutex
}

func NewState(title string, w, h, scale float32, fullscreen bool) *State {
	return &State{
		title:      title,
		width:      w,
		height:     h,
		scale:      scale,
		fullscreen: fullscreen,
	}
}

func (c *State) Size() use.Rect {
	c.mux.RLock()
	size := use.R(0, 0, c.width, c.height)
	c.mux.RUnlock()
	return size
}

func (c *State) Scale() float32 {
	c.mux.RLock()
	scale := c.scale
	c.mux.RUnlock()
	return scale
}

func (c *State) Fullscreen() bool {
	c.mux.RLock()
	fullscreen := c.fullscreen
	c.mux.RUnlock()
	return fullscreen
}

func (c *State) Title() string {
	c.mux.RLock()
	title := c.title
	c.mux.RUnlock()
	return title
}

func (c *State) Sync(screen *ebiten.Image) {
	w, h := screen.Size()
	scale := ebiten.ScreenScale()
	fullscreen := ebiten.IsFullscreen()
	c.mux.Lock()
	c.width = float32(w)
	c.height = float32(h)
	c.scale = float32(scale)
	c.fullscreen = fullscreen
	c.mux.Unlock()
}
